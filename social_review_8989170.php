<?php
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST');
	$location = $_REQUEST['loc'];
	$location = str_replace("*", "/", $location);
	$location = str_replace("|", "?", $location);
	$location = str_replace("^", "&", $location);
	$location = str_replace(" ", "+", $location);
	$location = str_replace("@", "=", $location);
	
	function get_domain($url)
	{
		$pieces = parse_url($url);
		$domain = isset($pieces['host']) ? $pieces['host'] : '';
		if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) 
		{
			return $regs['domain'];
		}
		return false;
	}
	
	function scrapeContentUsingCurl($productPageUrl,$username,$password) {
		$curl = curl_init($productPageUrl);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($curl, CURLOPT_USERPWD, $username . ":" . $password); //Your credentials goes here
		$pageContent = curl_exec($curl);
		if (curl_errno($curl)) { // check for execution errors
			echo 'Scraper error: ' . curl_error($curl);
			exit;
		}
		curl_close($curl);
		return $pageContent;
	}

	$domain_name = get_domain($location); // outputs 'somedomain.co.uk'
	//echo $domain_name;
   
	if($domain_name == "600social.com")
	{
		$username = 'social';
		$password = 'social1234';

		$context = stream_context_create(array(
		'http' => array(
		'header'  => "Authorization: Basic " . base64_encode("$username:$password")
		)
		));
		$html = file_get_contents($location,false,$context);
	}
	elseif($domain_name == "beyondyoga.net")
	{
		$uname="admin";
		$pass="5w5VT3P791Pn3XK";
		$html = scrapeContentUsingCurl($location,$uname,$pass);
		
	}else{
		$html = file_get_contents($location,false,$context);
	}

	//////////////////////////////Product Name//////////////////////////////////////

	preg_match_all('/var sa_s28_product_name\s{0,}=\s{0,}"(.*?)";/s',$html,$result);

	$product_name_1 = strip_tags($result[1][0]);

	$product_name = $product_name_1;			

	//////////////////////////////Product Price//////////////////////////////////////
	preg_match_all('/<span class="price">(.*?)<\/span>/s',$html,$resultprice);
	//print_r($resultprice);
    $js_product_price1 =  trim($resultprice[1][0]);
	$js_product_price2 = explode("$",$js_product_price1);
	$js_product_price = $js_product_price2[1];
	///////////////////////////Product Description//////////////////////////////////

	preg_match_all('/<meta name="description" content="(.*?)"/s',$html,$resultdesc);

	$prod_desc1 = $resultdesc[1][0];
	$product_description = strip_tags($prod_desc1);					
    if($product_description == "")
	{
		$product_description = "Beyond Yoga";
	}
	$product_description=substr($product_description,0,80)."...";

	/////////////////////////////Product Image Path//////////////////////////////////
	
	
	preg_match_all('/var sa_s28_product_image_url\s{0,}=\s{0,}"(.*?)"/s',$html,$resulti);
	//$prod_image ="http://cdn.socialannex.com/custom_images/5597980/A3ABYF_logo.jpg";
	$prod_image =trim($resulti[1][0]);


	/////////////////////////////////Product Id////////////////////////////////////	
				
	preg_match_all('/var sa_s28_product_id\s{0,}=\s{0,}"(.*?)";/s',$html,$resultid);
	$product_id = $resultid[1][0];;
	
	///////////////////////////////////////////////////////////////////////////////

		echo "var s28_scrp_product_name ='".$product_name."';";
		echo "var s28_scrp_product_desc ='".$product_description."';";
		echo "var s28_scrp_product_image ='".$prod_image."';";
		echo "var s28_scrp_product_price ='".$js_product_price."';";
        echo "var s28_scrp_product_id ='".$product_id."';";		
?>