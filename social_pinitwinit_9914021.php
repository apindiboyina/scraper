<?php
header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST');
	if(($_REQUEST["loc"]!=""))
	{
		$jsCall =1;
		$loc=$_REQUEST["loc"];
		getConfirmationVars($jsCall,$loc);
	}//if CLose.

	/**	
	  @author GK001
	  Modified by 6/12/2015
	  Removed file_get_contents instead of url_get_contents function.	
	**/
	function url_get_contents($url) {			
		$sa_s17_ch = curl_init();
		curl_setopt($sa_s17_ch,CURLOPT_URL, $url);
		curl_setopt($sa_s17_ch,CURLOPT_RETURNTRANSFER,1);
		$result = curl_exec($sa_s17_ch); 
		curl_close($sa_s17_ch);
	   
	   return $result;
	 }

	function getConfirmationVars($jsCall,$loc)
	{
		if($jsCall == '1')
		{

			$location = $loc;

			$location = str_replace("*", "/", $location);
			$location = str_replace("|", "?", $location);
			$location = str_replace("^", "&", $location);
			$location = str_replace(" ", "%20", $location);
			
			// Read Location 
			$product_price = "";
			//$html = file_get_contents($location);
			$html = url_get_contents($location);

			
     //////////////////////////////Product Name//////////////////////////////////////
			preg_match_all('/<span class="brand" itemprop="brand">(.*?)<\/span>/s',$html,$result);
			preg_match_all('/<span class="name" itemprop="name">(.*?)<\/span>/s',$html,$result1);
			$product_name_1 =  trim($result[1][0]);
			$product_name_2 =  trim($result1[1][0]);
			$product_name = $product_name_1 ." ".$product_name_2;				

			//////////////////////////////Product Price//////////////////////////////////////
		
			preg_match_all('/<span class="product_price ">(.*?)<\/span>/s',$html,$resultp);
			$prod_price1 =  trim(strip_tags($resultp[1][0]));
			$product_price =  $prod_price1;	
			$j_price2 = explode("$", $product_price);
			$js_product_price = $j_price2[1];			
			
			///////////////////////////Product Description//////////////////////////////////

			preg_match_all('/<meta name="Description" content="(.*?)"\/>/s',$html,$resultdesc);
			//preg_match_all('/<div class="product_desc">(.*?)<\/div>/s',$html,$resultdesc);
			$product_description2 = str_replace("<br/>"," &#124; ",$resultdesc[1][0]);
			$product_description3 = str_replace("<br />"," &#124; ",$product_description2);
			$product_description4 = str_replace("?"," &#124; ",$product_description3);
			$product_description = str_replace("'",'',$product_description4);


			if($product_description == "")
			{
				$product_description = "Your trusted source for the latest fashion apparel, shoes and accessories for women and men. Huge selection of kids sneakers, too.";
			}
			
			$product_description = substr($product_description, 0, 120)."...";
			
			/////////////////////////////Product Image Path//////////////////////////////////
			
			
					$domain = $_SERVER['HTTP_HOST'];		
					
					preg_match_all('/<img class="product_image" src="(.*?)"/s',$html,$resulti);							
					$prod_image1 = $resulti[1][0];	
					
					if($location != ""){					       
							$pieces = explode(".com", $location);									
							if($pieces[0] == "http://www.jimmyjazz"){
								$base = "";
							} 
							if($pieces[0] == "http://ecommerce.notbornhere"){
								$base="http://www.jimmyjazz.com";
							}	
							if($pieces[0] == "http://staging4.jimmyjazzstore.gil.avatarnewyork"){
								$base= $pieces[0].".com";
							}							
					}else {										
							if($domain=="http://www.jimmyjazz.com")            {				
							$base="http://www.jimmyjazz.com";
							}            else            {
							$base="http://staging4.jimmyjazzstore.gil.avatarnewyork.com";
							}			
					}			
					
					$prod_image1=$prod_image1;
						
					if($prod_image1=="")			{
						$prod_image = 'http://cdn.socialannex.com/custom_images/9914021/XDRHCA_logo100x100.png';
					}
						
			
					$prod_thum_image = $prod_image1;



			/////////////////////////////////Product Id////////////////////////////////////	

			/*preg_match_all('/<span class="pistylevalue".*?>\s{0,}(.*?)\s{0,}<\/span>/s',$html,$resultID1);            
			$product_id=$resultID1[1][0];*/
			/*preg_match_all('/<span class="pistylevalue".*?>\s{0,}(.*?)\s{0,}<\/span>/s',$html,$resultID1); */
			
			
			if(preg_match_all('/<span class="pistylevalue" style="display: inline;">\s{0,}(.*?)\s{0,}<\/span>/s',$html,$resultID1))
			{
			$product_id=$resultID1[1][0];
			}
			else if(preg_match_all('/<span class="pistylevalue" style="display: none;">\s{0,}(.*?)\s{0,}<\/span>/s',$html,$resultID1))
			{
				preg_match_all('/<span itemprop="sku" class="piskuvalue" style="display: inline;">\s{0,}(.*?)\s{0,}<\/span>/s',$html,$resultID2);
				$product_id=$resultID2[1][0];
			}
			
			
			
			///////////////////////////////////////////////////////////////////////////////

			$scrp_product_name = trim($product_name);
			$scrp_product_desc = substr($product_description, 0, 120)."...";
			$scrp_product_image = $prod_thum_image;
			$js_scrp_product_price = $js_product_price;
			$scrp_product_price = $product_price;
			$scrp_product_id = $product_id;
			$scrp_landing_url = $loc;
			
			echo "var s17_scrp_product_name='".$scrp_product_name."';";
 

			if($scrp_product_price != "")
			{
				echo "var s17_scrp_product_price='".$scrp_product_price."';";
				echo "var s17_js_scrp_product_price='".$js_scrp_product_price."';";
			}
			else
			{
				echo "var s17_scrp_product_price='0';";
			}


			if($scrp_product_desc != "")
			{
				
				
				$temp_desc = nl2br($scrp_product_desc);
				$temp_desc = trim($temp_desc);
				
				$block = array('<br>', '<br />', '<br /><br />', '<br /> ', ' <br /> ','<br /> ');
				$pp_desc = str_replace($block," ",$temp_desc);
				$ppp_desc = preg_replace('/\s\s+/', ' ',$pp_desc);

				$pppp_desc = substr($ppp_desc, 0, 120);
				$pppp_desc.="...";

				echo "var s17_scrp_product_desc='".$scrp_product_name."';";
				
				
			}



			echo "var s17_scrp_product_image='".trim($scrp_product_image)."';";

			echo "var s17_scrp_product_id='".trim($scrp_product_id)."';";
					
		}
		
	}//end funciton
?>