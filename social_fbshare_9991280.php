<?php
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST');
	$location = $_REQUEST['loc'];
	$location = str_replace("*", "/", $location);
	$location = str_replace("|", "?", $location);
	$location = str_replace("^", "&", $location);
	$location = str_replace(" ", "+", $location);
	$location = str_replace("@", "=", $location);
	function get_domain($url)
	{
		$pieces = parse_url($url);
		$domain = isset($pieces['host']) ? $pieces['host'] : '';
		if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) 
		{
			return $regs['domain'];
		}
		return false;
	}
	$domain_name = get_domain($location); // outputs 'somedomain.co.uk'
	if($domain_name == "600social.com")
	{
		$username = 'social';
		$password = 'social1234';
		$context = stream_context_create(array(
		'http' => array(
		'header'  => "Authorization: Basic " . base64_encode("$username:$password")
		)
		));
		$html = file_get_contents($location,false,$context);
	}
	else
	{
		//echo "file content html".$html;exit;
		$html = file_get_contents($location,false,$context);
	}
	//////////////////////////////Product Name//////////////////////////////////////
	preg_match_all('/<meta property="og:title" content="(.*?)"/s',$html,$result);
	
	$product_name_1 =  trim($result[1][0]);	
	$product_name = $product_name_1;
	if($product_name_1=="")
	{
		$product_name = "Ukies";
	}
	//////////////////////////////Product Price//////////////////////////////////////
	preg_match_all('/<div class="price">(.*?)<\/div>/s',$html,$resultprice);
	$product_price =  $resultprice[1][0];
	$js_product_price1 = strip_tags($product_price);
	$js_product_price = trim($js_product_price1);
	if($js_product_price==""){
		$js_product_price = "00.00";
	}
	///////////////////////////Product Description//////////////////////////////////
	preg_match_all('/<div class="productdescription">(.*?)<\/div>/s',$html,$resultdesc);
	$product_description = $resultdesc[1][0];
	$product_description1 = substr($product_description, 0, 120);
	$product_description = trim($product_description1);
	if($product_description==""){
		$product_description="Luxurious european made shoesthat keep you on your feet all day";
	}
	/////////////////////////////Product Image Path//////////////////////////////////	
	preg_match_all('/<meta property="og:image" content="(.*?)"/s',$html,$resulti);			
	$prod_image1 =$resulti[1][0];
	$prod_image = $prod_image1;
	if($prod_image=="")
	{
		$prod_image = 'http://cdn.socialannex.com/custom_images/9991280/IRT6TB_ukies_logo.png';
	}
	/////////////////////////////////Product Id////////////////////////////////////				
	preg_match_all('/<meta property="og:image" content="(.*?)"/s',$html,$resultid);
	$product_id = $resultid[1][0];
	$prod_split = str_replace(["/",":","."] ,["","",""],$product_id);
	$prod_split2 = filter_var($prod_split, FILTER_SANITIZE_NUMBER_INT);
	$product_id = $prod_split2;
	if($product_id == ""){
		$product_id = "No_product_Info";
	}
	///////////////////////////////////////////////////////////////////////////////
	echo "var s28_scrp_product_name ='".$product_name."';";
	echo "var s28_scrp_product_desc ='".$product_description."';";
	echo "var s28_scrp_product_image ='".$prod_image."';";
	echo "var s28_scrp_product_price ='".$js_product_price."';";
	echo "var s28_scrp_product_id ='".$product_id."';";		
?>