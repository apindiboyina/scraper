<?php
	if(($_REQUEST["loc"]!=""))
	{
		$jsCall =1;
		getConfirmationVars($jsCall,$loc);
	}//if CLose.

	function getConfirmationVars($jsCall,$loc)
	{
		if($jsCall == '0')
		{
			$location = $loc;

			$location = str_replace("*", "/", $location);
			$location = str_replace("|", "?", $location);
			$location = str_replace("^", "&", $location);

			// Read Location
			$product_price = "";
			$html = file_get_contents($location);

			//////////////////////////////Product Name//////////////////////////////////////
			preg_match_all('/<title>(.*?)<\/title>/s',$html,$result);
			
			$product_name_1 =  trim($result[1][0]);
			$product_name = $product_name_1;	

			//////////////////////////////Product Price//////////////////////////////////////
			
			preg_match_all('/<title>(.*?)<\/title>/s',$html,$result);
			
			$product_name_1 = trim($result[1][0]);
			$product_name = $product_name_1;	

			//////////////////////////////Product Price//////////////////////////////////////
			preg_match_all('/<input type="hidden" name="product" value="(.*?)" \/>/s',$html,$resultpid);
		
			$prodid = $resultpid[1][0];
			
			$exp1 = '/<span class="regular-price" id="product-price-">(.*?)<\/span>/s';
			$exp8 = '/<span class="regular-price" id="product-price-'.$prodid.'">(.*?)<\/span>/s';
			$exp3 = '/<span class="special-price" id="product-price-'.$prodid.'-min">(.*?)<\/span>/s';
			$exp4 = '/<span class="special-price">\\s+<span class="price" id="product-price-'.$prodid.'-max">(.*?)<\/span>/s';
			$exp2 = '/<span class="price" id="product-price-'.$prodid.'">(.*?)<\/span>/s';
			$exp5 = '/<span class="price" id="product-price-'.$prodid.'-min">(.*?)<\/span>/s';
			$exp6 = '/<span class="regular-price" id="product-price-'.$prodid.'-min">(.*?)<\/span>/s';            
			$exp7 = '/<span class="price" id="product-price-'.$prodid.'-max">(.*?)<\/span>/s';
			
			if(preg_match_all($exp8,$html,$resultsingle))
			{
			   preg_match_all($exp8,$html,$resultsingle);
			  
			   $amount = explode("$",$resultsingle[0][0]);
			 
			   $product_price = $amount[1];
		
			   $js_product_price = $amount[1];
			}
			else if(preg_match_all($exp1,$html,$resultsingle))
			{
			  preg_match_all($exp1,$html,$resultsingle);
			  
			  $amount = explode("$",$resultsingle[0][0]);
			 
			  $product_price = $amount[1];
		
			  $js_product_price = $amount[1];
			}
			else if(preg_match_all($exp3,$html,$resultrange))
			{
				preg_match_all($exp3,$html,$resultrange);
			
				preg_match_all($exp4,$html,$resultrange1);
				
				$minprice = explode("$", $resultrange[0][0]);
				
				$maxprice = explode("$", $resultrange1[0][0]);
				
				$finalprice = $minprice[1]."-".$maxprice[1];
								
				$product_price = $finalprice;
			
				$js_product_price = $finalprice;
				
			}
			else if(preg_match_all($exp2,$html,$resultstrikeout))
			{
			 	preg_match_all($exp2,$html,$resultstrikeout);

                $amount = explode("$",$resultstrikeout[0][0]);
				
				$product_price = $amount[1];
		
			    $js_product_price = $amount[1];
			}
			else if(preg_match_all($exp5,$html,$resultsinglestrike))
			{
			 	preg_match_all($exp5,$html,$resultsinglestrike);

                $amount = explode("$",$resultsinglestrike[0][0]);
				
				$product_price = $amount[1];
		
			    $js_product_price = $amount[1];
			}
			else if(preg_match_all($exp6,$html,$resultrangestrike))
			{
			    preg_match_all($exp6,$html,$resultrangestrike);
				
				preg_match_all($exp7,$html,$resultrangestrike1);
				
				$minprice = explode("$", $resultrangestrike[0][0]);
				
				$maxprice = explode("$", $resultrangestrike1[0][0]);
				
				$finalprice = $minprice[1]."-".$maxprice[1];
								
				$product_price = $finalprice;
			
				$js_product_price = $finalprice;
			}
					
			///////////////////////////Product Description//////////////////////////////////

			preg_match_all('/<meta name="description" content="(.*?)"/s',$html,$result);
			
			$dresult = $result[1][0];
			$dresult1 = htmlspecialchars_decode($dresult);
			$description = substr($dresult1,0,120);
			$product_description = strip_tags($description);
			
			if($product_description == "")
			{
				$product_description = "The Gemstone Destination – Best Quality Gemstone &amp; Diamond Jewelry for the glamor of life. Enjoy fine jewelry at great prices with Free Shipping and 30 day easy returns.";
			}

			/////////////////////////////Product Image Path//////////////////////////////////
			
			preg_match_all('/<div class="video-playlist">.*?<img src="(.*?)"/s',$html,$resultimg1);
			
			$img2 = $resultimg1[1][0];
		
			$prod_thum_image = $img2;

			/////////////////////////////////Product Id////////////////////////////////////	
			preg_match_all('/<input type="hidden" name="product" value="(.*?)" \/>/s',$html,$resultpid);
			
			//preg_match_all("/pr_page_id: '(.*?)'/s",$html,$resultid);
		  
			$pid1=$resultpid[1][0];
		     
			$product_id =$pid1;
			
			///////////////////////////////////////////////////////////////////////////////
			if(strlen(addslashes(trim($product_name))) > 50)
			{
				$product_name = substr($product_name ,0 ,50);
				$product_name = $product_name.'...';
			}
			$scrp_product_name = addslashes(trim($product_name));
			$scrp_product_desc = $product_description;
			$scrp_product_image = $prod_thum_image;
			$js_scrp_product_price = $js_product_price;
			$scrp_product_price = $product_price;
			$scrp_product_id = $product_id;
			$scrp_landing_url = $loc;
			

			$build = $scrp_product_name."^".$scrp_product_desc."^".$scrp_product_image."^".$scrp_product_price."^".$scrp_product_id."^".$scrp_landing_url;

			return $build; 		
		}
		if($jsCall != '0')
		{
?>
		var loca = document.location.href;
		var scrp_custom_var	= loca;

		var scrp_product_name	= '';
		var scrp_product_desc	= '';
		var scrp_product_image	= '';
		var scrp_product_price	= '';
		var scrp_product_id		= '';
		var scrp_landing_url	= '';
		var js_scrp_product_price = '1';
<?php
		}//if
	}//end funciton
?>