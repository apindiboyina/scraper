<?php
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST');
	if(($_REQUEST["loc"]!=""))
	{
		$jsCall =1;
		$loc=$_REQUEST["loc"];
		getConfirmationVars($jsCall,$loc);
	}//if CLose.
	function getConfirmationVars($jsCall,$loc)
	{
		if($jsCall == '1')
		{
			$location = $loc;

			$location = str_replace("*", "/", $location);
			$location = str_replace("|", "?", $location);
			$location = str_replace("^", "&", $location);
			

			// Read Location 
			$product_price = "";
			$html = file_get_contents($location);

            $html =stripslashes($html);

		    //////////////////////////////Product name//////////////////////////////////////
			preg_match_all('/<meta property="og:title" content="(.*?)"/s',$html,$result);
			$product_name_13 =  trim($result[1][0]);
			$product_name = $product_name_13;
			$product_name = str_replace("&#039;", "'", $product_name);
			
			///////////////////////////////Left Side Title/////////////////////////////////

			preg_match_all('/<div class="artist left">(.*?)<\/div>/s',$html,$resultname1);
			//print_r($resultname1[1][0]);exit;
			preg_match_all('/<span class="shirt_title">(.*?)<\/span>/s',$resultname1[1][0],$prod_1);
			$left_product1=$prod_1[1][0];
			$left_product= html_entity_decode($left_product1, ENT_QUOTES);
			//$left_product = str_replace("'", "", $left_product2);
			preg_match_all('/<span class="by_link_left">(.*?)<\/span>/s',$resultname1[1][0],$prodauthor_1);
			$left_author=strip_tags($prodauthor_1[1][0]);
			
			$product_name1=$left_product." ".$left_author;
			///////////////////////////////End Left Side Title/////////////////////////////////
			///////////////////////////////Right Side Title/////////////////////////////////
			preg_match_all('/<div class="artist right">(.*?)<\/div>/s',$html,$resultname2);
			//print_r($resultname2[1][0]);exit;
			preg_match_all('/<span class="shirt_title">(.*?)<\/span>/s',$resultname2[1][0],$prod_2);
			$right_product1=$prod_2[1][0];
			$right_product= html_entity_decode($right_product1, ENT_QUOTES);
			//$right_product = str_replace("'", "", $right_product2);
			preg_match_all('/<span class="by_link_right">(.*?)<\/span>/s',$resultname2[1][0],$prodauthor_2);
			$right_author=strip_tags($prodauthor_2[1][0]);
			
			$product_name2=$right_product." ".$right_author;
			//////////////////////////////Product Price//////////////////////////////////////
			
			$product_price ='0';		
			$product_price1 = '0';		
			$product_price2 = '0';	

			///////////////////////////Product Description//////////////////////////////////

			$product_description = "Wear your art on your sleeve with 24-hour limited edition tees for only $11 from TeeFury.";
			$product_description1 = "Wear your art on your sleeve with 24-hour limited edition tees for only $11 from TeeFury.";
			$product_description2 = "Wear your art on your sleeve with 24-hour limited edition tees for only $11 from TeeFury.";
			/////////////////////////////////Product Id////////////////////////////////////
			//----------  Updated by PB01 ----------------------------------//
			preg_match_all('/<input type="hidden" id="product_id" value="(.*?)"/s',$html,$resultid);
			$product_id1 = $resultid[1][0];
			$product_id=$product_id1;
			
			if($product_id == "")
			{
				preg_match_all('/<input type="hidden" name="product_id" id="social_product_id" value="(.*?)"/s',$html,$resultidsecond);
				$product_id2 = $resultidsecond[1][0];
				$product_id=$product_id2;
			}
			if($product_id == "")
			{
				$product_id = "noproductinfo";
			}
			
			/////////////////////////////////TwoFury Product Ids/////////////////////////
			
			preg_match_all('/<input type="hidden" name="product_id_a" id="product_id_a" value="(.*?)"/s',$html,$resultid1);
			$product_id11 = $resultid1[1][0];
			$product_id1=$product_id11;
			
			preg_match_all('/<input type="hidden" name="product_id_b" id="product_id_b" value="(.*?)"/s',$html,$resultid2);
			$product_id12 = $resultid2[1][0];
			$product_id2=$product_id12;
			//------------------------------------------------------------//
			/////////////////////////////////Product Image////////////////////////////////////
			preg_match_all('/<div id="zoom-pic" class="main-artwork-image-container" style="(.*?)">(.*?)<\/div>/s',$html,$resulti);
		   
			$prod_image =$resulti[0][0];
			$prod_image2 = explode('<img src="',$prod_image);
			$prod_image3 = explode('"',$prod_image2[1]);
			$prod_image4=$prod_image3[0];
			$prod_thum_image = str_replace("products_large_images","products_large_images_flat",$prod_image4);
			
			preg_match_all('/<div class="colors-tst-left-img-container".*?>\s{0,}<img src="(.*?)"/s',$html,$resulti1);
			$prod_image1 =$resulti1[1][0];
			$prod_thum_image1 = str_replace("products_large_images","products_large_images_flat",$prod_image1);
			
			preg_match_all('/<div class="colors-tst-right-img-container".*?>\s{0,}<img src="(.*?)"/s',$html,$resulti2);
			$prod_image2 =$resulti2[1][0];
			$prod_thum_image2 = str_replace("products_large_images","products_large_images_flat",$prod_image2);
			///////////////////////////////////////////////////////////////////////////////
			
			$scrp_product_name = trim(addslashes($product_name));
			$scrp_product_desc =   substr($product_description, 0, 120);
			$scrp_product_image =$prod_thum_image;
			$js_scrp_product_price = $product_price;
			$scrp_product_price = $product_price;
			$scrp_product_id = $product_id;
			$scrp_landing_url = $loc;
			
			$scrp_product_name1 = trim(addslashes($product_name1));
			$scrp_product_desc1 =   substr($product_description1, 0, 120);
			$scrp_product_image1 =$prod_thum_image1;
			$js_scrp_product_price1 = $product_price1;
			$scrp_product_price1 = $product_price1;
			$scrp_product_id1 = $product_id1;
			$scrp_landing_url1 = $loc;
			
			$scrp_product_name2 = trim(addslashes($product_name2));
			$scrp_product_desc2 =   substr($product_description2, 0, 120);
			$scrp_product_image2 =$prod_thum_image2;
			$js_scrp_product_price2 = $product_price2;
			$scrp_product_price2 = $product_price2;
			$scrp_product_id2 = $product_id2;
			$scrp_landing_url2 = $loc;

			echo "var s25_scrp_product_name='".$scrp_product_name."';";
			if($scrp_product_price != "")
			{
				echo "var s25_scrp_product_price='".$scrp_product_price."';";
				echo "var s25_js_scrp_product_price='".$js_scrp_product_price."';";
			}
			else
			{
				echo "var s25_scrp_product_price='0';";
			}
			if($scrp_product_desc != "")
			{
				
				
				$temp_desc = nl2br($scrp_product_desc);
				$temp_desc = trim($temp_desc);
				
				$block = array('<br>', '<br />', '<br /><br />', '<br /> ', ' <br /> ','<br /> ');
				$pp_desc = str_replace($block," ",$temp_desc);
				$ppp_desc = preg_replace('/\s\s+/', ' ',$pp_desc);

				$pppp_desc = substr($ppp_desc, 0, 120);
				$pppp_desc.="...";

				echo "var s25_scrp_product_desc='".trim(addslashes($pppp_desc))."';";
				
			 }
			echo "var s25_scrp_product_image='".trim($scrp_product_image)."';";

			echo "var s25_scrp_product_id='".trim($scrp_product_id)."';";
			
			
			//////////////////////////////////Left/////////////////////////
			
			echo "var s25_scrp_product_name1='".$scrp_product_name1."';";
			if($scrp_product_price != "")
			{
				echo "var s25_scrp_product_price1='".$scrp_product_price1."';";
				echo "var s25_js_scrp_product_price1='".$js_scrp_product_price1."';";
			}
			else
			{
				echo "var s25_scrp_product_price1='0';";
			}
			if($scrp_product_desc1 != "")
			{
				
				
				$temp_desc = nl2br($scrp_product_desc1);
				$temp_desc = trim($temp_desc);
				
				$block = array('<br>', '<br />', '<br /><br />', '<br /> ', ' <br /> ','<br /> ');
				$pp_desc = str_replace($block," ",$temp_desc);
				$ppp_desc = preg_replace('/\s\s+/', ' ',$pp_desc);

				$pppp_desc = substr($ppp_desc, 0, 120);
				$pppp_desc.="...";

				echo "var s25_scrp_product_desc1='".trim(addslashes($pppp_desc))."';";
				
			 }
			echo "var s25_scrp_product_image1='".trim($scrp_product_image1)."';";

			echo "var s25_scrp_product_id1='".trim($scrp_product_id1)."';";
			
			////////////////////////////////Right///////////////////////////////
			echo "var s25_scrp_product_name2='".$scrp_product_name2."';";
			if($scrp_product_price != "")
			{
				echo "var s25_scrp_product_price2='".$scrp_product_price2."';";
				echo "var s25_js_scrp_product_price2='".$js_scrp_product_price2."';";
			}
			else
			{
				echo "var s25_scrp_product_price2='0';";
			}
			if($scrp_product_desc2 != "")
			{
				
				
				$temp_desc = nl2br($scrp_product_desc2);
				$temp_desc = trim($temp_desc);
				
				$block = array('<br>', '<br />', '<br /><br />', '<br /> ', ' <br /> ','<br /> ');
				$pp_desc = str_replace($block," ",$temp_desc);
				$ppp_desc = preg_replace('/\s\s+/', ' ',$pp_desc);

				$pppp_desc = substr($ppp_desc, 0, 120);
				$pppp_desc.="...";

				echo "var s25_scrp_product_desc2='".trim(addslashes($pppp_desc))."';";
				
			 }
			echo "var s25_scrp_product_image2='".trim($scrp_product_image2)."';";

			echo "var s25_scrp_product_id2='".trim($scrp_product_id2)."';";
					
		}
		
	}//end function
?>