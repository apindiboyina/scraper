<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');

			$location = $_REQUEST["loc"];

			$location = str_replace("*", "/", $location);
			$location = str_replace("|", "?", $location);
			$location = str_replace("^", "&", $location);
			$location = str_replace(" ", "+", $location);
			$location = str_replace("@", "=", $location);


			// Read Location 
			$product_price = "";

			function get_domain($url)
			{
				$pieces = parse_url($url);
				$domain = isset($pieces['host']) ? $pieces['host'] : '';
				if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) 
				{
					return $regs['domain'];
				}
				return false;
			}

		/**	
		  @author GK001
		  Modified by 6/12/2015
		  Removed file_get_contents instead of url_get_contents function.	
		**/
		function url_get_contents($url) {			
			$sa_s28_ch = curl_init();
			curl_setopt($sa_s28_ch,CURLOPT_URL, $url);
			curl_setopt($sa_s28_ch,CURLOPT_RETURNTRANSFER,1);
			$result = curl_exec($sa_s28_ch); 
			curl_close($sa_s28_ch);
		   
		   return $result;
		 }

			$domain_name = get_domain($location); // outputs 'somedomain.co.uk'
		   
			if($domain_name == "600social.com")
			{
				$username = 'social';
				$password = 'social1234';

				$context = stream_context_create(array(
				'http' => array(
				'header'  => "Authorization: Basic " . base64_encode("$username:$password")
				)
				));
				$html = file_get_contents($location,false,$context);
			}
			else
			{
				//$html = file_get_contents($location);
				$html = url_get_contents($location);
			}

	//////////////////////////////Product Name//////////////////////////////////////
    //<div class="detailheader">
	preg_match_all('/<div class="detailheader">(.*?)<\/div>/s',$html,$result);
   
	$product_name_1 =  trim($result[1][0]);
	$product_name = strip_tags($product_name_1);			

	//////////////////////////////Product Price//////////////////////////////////////
	if(preg_match_all('/<span itemprop="price">(.*?)<\/span>/s',$html,$resultprice))
        { 
        $js_product_price1 =  trim($resultprice[1][0]);
		//$js_product_price2 = explode("$",$js_product_price1);
		$js_product_price = $js_product_price1;
        }
        else
        {
        preg_match_all('/<div class="price-info">(.*?)<\/div>/s',$html,$resultprice);
        
        preg_match_all('/<span class="regular-price" .*?>(.*?)<\/span>/s',$resultprice[1][0],$resultprice1);
        $product_id = $resultprice[1][0];
        $js_product_price1 =  strip_tags($product_id);
		$js_product_price2 = explode("$",$js_product_price1);
		$js_product_price = trim($js_product_price2[1]);         
        }

	///////////////////////////Product Description//////////////////////////////////
	preg_match_all('/<meta name="description" content="(.*?)"/s',$html,$resultdesc);
	$prod_desc1 = $resultdesc[1][0];
	$product_description = trim($prod_desc1);					
    if($product_description == "")
	{
		$product_description = "This raglan is anything but ordinary. With trendy stripes and lace detail at shoulder, it's a great way to refresh your everyday look.";
	}
	$product_description=substr($product_description,0,120)."...";

	/////////////////////////////Product Image Path//////////////////////////////////
	   preg_match_all('/<div class="detailImage">(.*?)<\/div>/s',$html,$resulti);
        
            $prod_image1 =$resulti[1][0];
            preg_match_all('/<img .*? src="(.*?)"/s',$prod_image1,$resulti2);
			
            $prod_image =strip_tags($resulti2[1][0]);
        

	/////////////////////////////////Product Id////////////////////////////////////	
		preg_match_all('/<div id="ProductCode" .*?>(.*?)<\/div>/s',$html,$resultid);
        $product_ids = $resultid[1][0];
        $product_id = trim($product_ids);
        
	///////////////////////////////////////////////////////////////////////////////

		echo "var s28_scrp_product_name ='".$product_name."';";
		echo "var s28_scrp_product_desc ='".$product_description."';";
		echo "var s28_scrp_product_image ='".$prod_image."';";
		echo "var s28_scrp_product_price ='".$js_product_price."';";
        echo "var s28_scrp_product_id ='".$product_id."';";		
?>