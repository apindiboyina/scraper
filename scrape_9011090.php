<?php
	echo "var scrp_product_name='Safety 1st Alpha Omega Elite Convertible Car Seat';";
	echo "var scrp_product_desc='Convertible car seat offers 3 modes of use for an infant, young baby or toddler. The Safety 1st Alpha Omega Elite Convertible Car Seat can be used in the rear-facing position for an infant or the forward facing position with a 5-point harness for an older baby and it converts to a belt-positioning booster for a toddler. ';";
	echo "var scrp_product_image='http://www.babyage.com/icons/localhost/products/large/l_22456milflipped.jpg';";
	echo "var scrp_product_price='$121.00';";
	echo "js_scrp_product_price='121.00';";
	echo "var scrp_product_id='62610';";
?>