<?php
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST');
	$location = $_REQUEST['loc'];
	$location = str_replace("*", "/", $location);
	$location = str_replace("|", "?", $location);
	$location = str_replace("^", "&", $location);
	$location = str_replace(" ", "+", $location);
	$location = str_replace("@", "=", $location);
        
        /*Remove unwanted parameter from product url after .aspx 
        
         * Code added by Sujeet Jaiswal
         * On Date : 23-05-2016
         */
        $pos=  strpos($location,".aspx");
        if(!empty($pos)){
            $location =substr($location,0,$pos+5);
        }
        function get_domain($url)
	{
		$pieces = parse_url($url);
		$domain = isset($pieces['host']) ? $pieces['host'] : '';
		if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) 
		{
			return $regs['domain'];
		}
		return false;
	}

	$domain_name = get_domain($location); // outputs 'somedomain.co.uk'
   
	if($domain_name == "600social.com")
	{
		$username = 'social';
		$password = 'social1234';

		$context = stream_context_create(array(
		'http' => array(
		'header'  => "Authorization: Basic " . base64_encode("$username:$password")
		)
		));
		$html = file_get_contents($location,false,$context);
	}
	else
	{
               //$html = file_get_contents($location);
                $ch = curl_init($location);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
                $html = curl_exec($ch);
                curl_close($ch);
                
	}

	//////////////////////////////Product Name//////////////////////////////////////

	preg_match_all('/<h1 itemprop="name">(.*?)<\/h1>/s',$html,$result);

	$product_name_1 =  trim($result[1][0]);		

	 $product_name = $product_name_1;			

	//////////////////////////////Product Price//////////////////////////////////////

	$js_product_price1 = preg_match_all('/sa_p_price: "(.*?)"/s',$html,$resultprice);
	$js_product_price = $resultprice[1][0];
	///////////////////////////Product Description//////////////////////////////////

	$res=preg_match_all('/<div id="pdTab1".*?>(.*?)<\/div>/s',$html,$resultdesc);
                                   
	$prod_desc1 = $resultdesc[1][0];
	$product_description = strip_tags(trim(addslashes($prod_desc1)));					
    if($product_description == "")
	{
		$product_description = "Exquisitely tailored and custom designed suits, shirts, blazers, trousers, tuxedos and accessories.  All made to measure, shipped to you free of charge and backed by our Flawless Fit Promise.";
	}
	$product_description=substr($product_description,0,120)."...";

	/////////////////////////////Product Image Path//////////////////////////////////
	preg_match_all('/sa_p_img_url: "(.*?)"/s',$html,$resulti);
	$prod_image =$resulti[1][0];
	if($prod_image==""){
		$prod_image="http://static.chaparral-racing.com/productimages/600/07129-002-XS.jpg";
	}

	/////////////////////////////////Product Id////////////////////////////////////	

		preg_match_all('/sa_p_id: "(.*?)"/s',$html,$resultid);
		$product_id = $resultid[1][0];
		
		if($product_id =="")
		{
			$product_id="321-2100";
		}
	
	///////////////////////////////////////////////////////////////////////////////

		echo "var s28_scrp_product_name ='".trim(addslashes($product_name))."';";
		if($product_description != "")
		{
			$temp_desc = nl2br($product_description);
			$temp_desc = trim($temp_desc);
			
			$block = array('<br>', '<br />', '<br /><br />', '<br /> ', ' <br /> ','<br /> ');
			$pp_desc = str_replace($block," ",$temp_desc);
			$pp_desc1 = str_replace("&#39;","'",$pp_desc);
			
			$ppp_desc = preg_replace('/\s\s+/', ' ',$pp_desc1);

			$pppp_desc = substr($ppp_desc, 0, 120);
			$pppp_desc.="...";

			echo "var s28_scrp_product_desc='".trim(addslashes($pppp_desc))."';";
		 }
		echo "var s28_scrp_product_image ='".$prod_image."';";
		echo "var s28_scrp_product_price ='".$js_product_price."';";
        echo "var s28_scrp_product_id ='".$product_id."';";		
?>