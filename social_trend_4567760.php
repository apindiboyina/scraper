<?php
	
			$location = $_REQUEST["loc"];

			$location = str_replace("*", "/", $location);
			$location = str_replace("|", "?", $location);
			$location = str_replace("^", "&", $location);
			$output= "";
			// Read Location 
			$product_price = "";
			
			$html = file_get_contents($location);
			
			//////////////////////////////Product Name//////////////////////////////////////

			preg_match_all('/<div class="pdp_item_title">(.*?)<\/div>/s',$html,$result);

			$product_name_1 =  trim($result[1][0]);
			
			$product_name_2 = str_replace("Cach&eacute;","Cache", $product_name_1);

			$product_name_3 = str_replace("&amp;","&", $product_name_2);

			$product_name_4 = str_replace("&#039;","", $product_name_3);

			$product_name = $product_name_4;		

			//////////////////////////////Product Price//////////////////////////////////////

			if(preg_match_all('/<div class="priceHeader">(.*?)PRICE&nbsp;/s',$html,$result9))
			 {   
					
				  $s1 = trim($result9[1][0]);
	
				  $s2 = explode('SALE',$s1);
	
				  $s3 = $s2[1];
	
				  $s4 = strip_tags($s3);
				  
				  $s5 = str_replace('&nbsp;YOUR','',$s4);
				  
				  $product_price = $s5;
				  
				  $j_price = ",";
				  $j_price1 = str_replace($j_price,'',$product_price);
				  $j_price2 = explode("$", $j_price1);
				
				  $js_product_price = $j_price2[1];
			 }
			 else
			 {
					preg_match_all('/<div class="priceHeader">(.*?)<\/div>/s',$html,$result10);  					
				   
				 	$s1 = trim($result10[1][0]);
	
					$s2 = strip_tags($s1);

				    $product_price = $s2;
				   
					$j_price = ",";
					$j_price1 = str_replace($j_price,'',$product_price);
					$j_price2 = explode("$", $j_price1);
				
					$js_product_price = $j_price2[1];
			 }
			
			///////////////////////////Product Description//////////////////////////////////

			preg_match_all('/<meta name="description" content="(.*?)"/s',$html,$result);

			
     		$product_description1 = $result[1][0];

			$product_description = $product_description1;       
			

			if($product_description == "")
			{
				$product_description = "Cache is a nationwide, mall-based specialty retailer of  lifestyle sportswear and dresses targeting style-conscious women between the ages of 25-45.";
			}

			/////////////////////////////Product Image Path//////////////////////////////////

		   preg_match_all('/var _images(.*?)large/s',$html,$matches);

			$prod_img1 = $matches[1][0];				
			
			$prod_img2 = explode('"',$prod_img1);

			$prod_thum_image = "http://www.cache.com/items/cache/images/xlarge/".$product_id."_".$prod_img2[1].".jpg";

			/////////////////////////////////Product Id////////////////////////////////////	
						
			preg_match_all('/<div class="pdp_item_id">(.*?)<\/div>/s',$html,$resultp);

			$product_id1 = $resultp[1][0];

			$product_id2 = explode("#",$product_id1);
            $product_id = $product_id2[1];
			
			///////////////////////////////////////////////////////////////////////////////
		

			$scrp_product_name = addslashes(trim($product_name));
			$scrp_product_desc = addslashes(substr($product_description, 0, 120));
			$scrp_product_image = $prod_thum_image;
			$js_scrp_product_price = trim($js_product_price);
			$scrp_product_price = $product_price;
			$scrp_product_id = $product_id;
			$scrp_landing_url = $location;		

			echo "var s14_scrp_product_name='".trim($scrp_product_name)."';";
			if($scrp_product_desc != "")
			{
				
				
				$temp_desc = nl2br($scrp_product_desc);
				$temp_desc = trim($temp_desc);
				
				$block = array('<br>', '<br />', '<br /><br />', '<br /> ', ' <br /> ','<br /> ');
				$pp_desc = str_replace($block," ",$temp_desc);
				$ppp_desc = preg_replace('/\s\s+/', ' ',$pp_desc);

				$pppp_desc = substr($ppp_desc, 0, 120);
				$pppp_desc.="...";

				echo "var s14_scrp_product_desc='".trim(addslashes($pppp_desc))."';";
				
				
			}

			if($scrp_product_price != "")
			{
				echo "var s14_scrp_product_price='".$scrp_product_price."';";
				echo "var s14_js_scrp_product_price='".$js_scrp_product_price."';";
			}
			else
			{
				echo "var s14_scrp_product_price='1';";
			}
			
			echo "var s14_scrp_product_image='".trim($scrp_product_image)."';";
			echo "var s14_scrp_product_id='".trim($scrp_product_id)."';";

?>