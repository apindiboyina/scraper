<?php

$location = $_REQUEST["loc"];

//echo $location."<br>";

$html = file_get_contents($location);

preg_match_all(
    '/<td class="pageHeading" valign="top">(.*?)<br>/s',
    $html,
    $result
);

$product_name = strip_tags($result[0][0]);

preg_match_all(
    '/<td class="pageHeading" align="right" valign="top">(.*?)<\/td>/s',
    $html,
    $result
);

$product_price = strip_tags($result[0][0]);

preg_match_all(
	'/<span class="productSpecialPrice">(.*?)<\/span>/s',
    $html,
    $result
);

$product_special_price = strip_tags($result[0][0]);

preg_match_all(
	'/(<img)\s (src="([a-zA-Z0-9\.;:\/\?&=_|\r|\n]{1,})")/isxmU',
    $html,
    $images
);

$product_image = "http://oscommerce.notbornhere.com/".$images[3][27];


echo "var scrp_product_name='".$product_name."';";
echo "var scrp_product_image='".$product_image."';";

if($product_special_price != "")
{
	echo "var scrp_product_price='".$product_special_price."';";
}
else
{
	echo "var scrp_product_price='".$product_price."';";
}

?>