<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');

			$location1 = explode("#",$_REQUEST["loc"]);
			$location = $location1[0];
			$location = $location;

			$location = str_replace("*", "/", $location);
			$location = str_replace("|", "?", $location);
			$location = str_replace("^", "&", $location);
			$location = str_replace("@", "=", $location);

            function remove_http($url) {
                $disallowed = array('http://', 'https://');
               foreach($disallowed as $d) {
                  if(strpos($url, $d) === 0) {
                      return str_replace($d, 'http://', $url);
                  }
               }
               return $url;
            }
           // $url  = 'https://dev01-us-traegergrills.demandware.net/s/Traeger/BAC025.html';
            $location = remove_http($location);

            
            
			// Read Location 
			$product_price = "";
			function get_domain($url)
			{
				$pieces = parse_url($url);
				$domain = isset($pieces['host']) ? $pieces['host'] : '';
				if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) 
				{
					return $regs['domain'];
				}
				return false;
			}
			
			function url_get_contents($location) {	
				
				$curl = curl_init($location);
				curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
				curl_setopt($curl, CURLOPT_USERPWD, "storefront:meatmadness"); //Your credentials goes here
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
				$result = curl_exec($curl);
				curl_close($curl);
			   
				return $result;
				
			}

			$domain_name = get_domain($location); // outputs 'somedomain.co.uk'
		   
			if($domain_name == "600social.com")
			{
				$username = 'social';
				$password = 'social1234';

				$context = stream_context_create(array(
				'http' => array(
				'header'  => "Authorization: Basic " . base64_encode("$username:$password")
				)
				));
				$html = file_get_contents($location,false,$context);
			}
			else
			{
				$html = url_get_contents($location);
			}

	//////////////////////////////Product Name//////////////////////////////////////
		if(preg_match_all('/<div class="recipe-info-top">(.*?)<\/div>/s',$html,$resultproname))
		{	
			$product_name1 = trim($resultproname[1][0]);
			preg_match_all('/<h1 class="recipe-name" .*?>(.*?)<\/h1>/s',$product_name1,$result);
			$product_name_2 =trim($result[1][0]);
			$product_name = $product_name_2;
		}
		else{
			preg_match_all('/<h1 class="product-name" .*?>(.*?)<\/h1>/s',$html,$resultproname);
			$product_name = trim($resultproname[1][0]);
			//$product_name = "Stop grilling, Start Traeger'ing!";	
			//echo "var s28_scrp_product_name='".$product_name."';";			
		}
	///////////////////////////Product Description//////////////////////////////////
		
		$product_description = "Traeger Wood Pellet Grills have been the outdoor cooking choice of food enthusiasts.";
		$product_description=substr($product_description,0,70);

	/////////////////////////////Product Image Path//////////////////////////////////
		if(preg_match_all('/sa_p_img_url: "(.*?)"/s',$html,$resultproimage))
		{
			$prod_thum_image1 = trim($resultproimage[1][0]);
			$prod_thum_image2 = explode("?", $prod_thum_image1);
			$prod_thum_image = $prod_thum_image2[0];
		}
		else{		  
			$prod_thum_image ="http://sits-pod29.demandware.net/dw/image/v2/AAXM_STG/on/demandware.static/Sites-Traeger-Site/Sites-traeger_master_catalog/default/dw7f18f0dc/images/BBQ075.04-1.jpeg";
		}
	   
	/////////////////////////////////Product Id////////////////////////////////////	
		if(preg_match_all('/<span itemprop="productID">(.*?)<\/span>/s',$html,$resultproid))
		{
			$product_id = trim($resultproid[1][0]);
			
		}
		elseif(preg_match_all('/var sa_s28_product_id = "(.*?)"/s',$html,$resultproid)){
		$product_id = trim($resultproid[1][0]);
		}
		else{		  
			$product_id ="noproductinfo";
		}

	//////////////////////////////Product Price//////////////////////////////////////
			preg_match_all('/<span class="price-sales">(.*?)<\/span>/s',$html,$resultp);
			$prod_price1 = $resultp[1][0];
			$product_price12 = explode("$",$prod_price1);
			$product_price = strip_tags($product_price12[1]);			
	////////////////////////////////////////////////////////////////////////////////////

			$scrp_product_name = trim($product_name);			
			$scrp_product_desc = $product_description;
			$scrp_product_image = $prod_thum_image;
			//$js_scrp_product_price = "1";
			$scrp_product_price = trim($product_price);
			$scrp_product_id = $product_id;
			$scrp_caption = 'www.traegergrills.com';
			
			
			if($scrp_product_name != "")
			{
				$temp_name = nl2br($scrp_product_name);
				$temp_name = trim($temp_name);
				
				$block = array('<br>', '<br />', '<br /><br />', '<br /> ', ' <br /> ','<br /> ');
				$pp_name = str_replace($block," ",$temp_name);
				$pp_name1 = str_replace("&#39;","'",$pp_name);
				//echo strip_tags($pp_name1);exit;
				$ppp_name = preg_replace('/\s\s+/', ' ',$pp_name1);

				//$pppp_desc = substr($ppp_desc, 0, 120);
				//$pppp_desc.="...";

				echo "var s28_scrp_product_name='".strip_tags($ppp_name)."';";
			 }
			if($scrp_product_desc != "")
			{
				$temp_desc = nl2br($scrp_product_desc);
				$temp_desc = trim($temp_desc);
				
				$block = array('<br>', '<br />', '<br /><br />', '<br /> ', ' <br /> ','<br /> ');
				$pp_desc = str_replace($block," ",$temp_desc);
				$pp_desc1 = str_replace("&#39;","'",$pp_desc);
			
				$ppp_desc = preg_replace('/\s\s+/', ' ',$pp_desc1);

				$pppp_desc = substr($ppp_desc, 0, 120);
				//$pppp_desc.="...";

				echo "var s28_scrp_product_desc='".trim(addslashes($pppp_desc))."';";
			 }
			
			//echo "var s28_scrp_product_name='".$scrp_product_name."';";

			//echo "var s28_scrp_product_desc='".$scrp_product_desc."';";

			echo "var s28_scrp_product_image='".$scrp_product_image."';";

			//echo "var js_scrp_product_price='".$js_scrp_product_price."';";

			echo "var scrp_product_price='".$scrp_product_price."';";

			echo "var s28_scrp_product_id='".$scrp_product_id."';";

			echo "var scrp_caption='".$scrp_caption."';";

	
?>