<?php
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST');
	$location = $_REQUEST['loc'];
	$location = str_replace("*", "/", $location);
	$location = str_replace("|", "?", $location);
	$location = str_replace("^", "&", $location);
	$location = str_replace(" ", "+", $location);
	$location = str_replace("@", "=", $location);
	
	function get_domain($url)
	{
		$pieces = parse_url($url);
		$domain = isset($pieces['host']) ? $pieces['host'] : '';
		if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) 
		{
			return $regs['domain'];
		}
		return false;
	}

	$domain_name = get_domain($location); // outputs 'somedomain.co.uk'
   
	if($domain_name == "600social.com")
	{
		$username = 'social';
		$password = 'social1234';

		$context = stream_context_create(array(
		'http' => array(
		'header'  => "Authorization: Basic " . base64_encode("$username:$password")
		)
		));
		$html = file_get_contents($location,false,$context);
	}
	else
	{
		$html = file_get_contents($location,false,$context);
	}

	//////////////////////////////Product Name//////////////////////////////////////

	preg_match_all('/<h1>(.*?)<\/h1>/s',$html,$result);

	$product_name_1 =  trim($result[1][0]);		

	$product_name = "LIGHT GRAY LINEN BLEND CUSTOM SUIT";			

	//////////////////////////////Product Price//////////////////////////////////////
	preg_match_all('/<span class="price">(.*?)<\/span>/s',$html,$resultprice);
	
    $js_product_price1 =  trim($resultprice[1][0]);
	$js_product_price2 = explode("$",$js_product_price1);
	$js_product_price = "499";
	///////////////////////////Product Description//////////////////////////////////

	preg_match_all('/<meta name="description" content="(.*?)"/s',$html,$resultdesc);

	$prod_desc1 = $resultdesc[1][0];
	$product_description = strip_tags($prod_desc1);					
    if($product_description == "")
	{
		$product_description = "The varying shades of gray threads in this fabric give this suit its textured look.";
	}
	$product_description=substr($product_description,0,120)."...";

	/////////////////////////////Product Image Path//////////////////////////////////
	
	
	preg_match_all('/<img id="zoom-container" class="cloudzoom" src="(.*?)"/s',$html,$resulti);
	$prod_image ="http://res-4.cloudinary.com/http-www-blacklapel-com/image/fetch/w_696/http://s3-us-west-2.amazonaws.com/blacklapel-ember-assets/assets/products/light-gray-linen-blend-suit-hero.jpg";
	/*Add code for getting product image while creating magento extension BY Sujeet Jaiswal on date 01-11-2014 */
	if($prod_image == ''){
		preg_match_all('/<a class="product-image image-zoom" id="main-image".*?href="(.*?)"/s',$html,$resulti);
	$prod_image =$resulti[1][0];
		
	}
	/*End Here*/

	/////////////////////////////////Product Id////////////////////////////////////	
				
	preg_match_all('/<input type="hidden" name="product" value="(.*?)"/s',$html,$resultid);
	$product_id = "19140";
	
	///////////////////////////////////////////////////////////////////////////////

		echo "var s28_scrp_product_name ='".$product_name."';";
		echo "var s28_scrp_product_desc ='".$product_description."';";
		echo "var s28_scrp_product_image ='".$prod_image."';";
		echo "var s28_scrp_product_price ='".$js_product_price."';";
        echo "var s28_scrp_product_id ='".$product_id."';";		
?>