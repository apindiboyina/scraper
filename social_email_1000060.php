<?php
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST');
	if(($_REQUEST["loc"]!=""))
	{
		$jsCall =1;
		$loc=$_REQUEST["loc"];
		getConfirmationVars($jsCall,$loc);
	}//if CLose.
	function getConfirmationVars($jsCall,$loc)
	{
		if($jsCall == '1')
		{
			$location = $loc;

			$location = str_replace("*", "/", $location);
			$location = str_replace("|", "?", $location);
			$location = str_replace("^", "&", $location);
			

			// Read Location 
			$product_price = "";		
			
			$html = file_get_contents($location);

		   //////////////////////////////Product name//////////////////////////////////////
			preg_match_all('/<title>(.*?)<\/title>/s',$html,$resultname);
			$product_name=$resultname[1][0];

			//////////////////////////////Product Price//////////////////////////////////////
			preg_match_all('/<div class="price-box">(.*?)<\/div>/s',$html,$resultp);
			$product_price =  trim(strip_tags($resultp[1][0]));
			$js_price=explode("$",$product_price);
			$js_product_price=$js_price[1];          
			
			///////////////////////////Product Description//////////////////////////////////
			preg_match_all('/<meta name="description" content="(.*?)"/s',$html,$result); 

			$product_description = strip_tags($result[1][0]);			
			
			if($product_description == "")
			{
				$product_description ="Special offers on 137 Pillars House packages - deals on hotel vacation packages and luxury hotels in Chiang Mai, Thailand.";
			}

			//////////////////////////////Product shot desc//////////////////////////////////////

			preg_match_all('/<title>(.*?)<\/title>/s',$html,$resultshortdesc);
			$product_short_desc=$resultshortdesc[1][0];


			/////////////////////////////////Product Id////////////////////////////////////
			preg_match_all('/<span id="sa_product_code">(.*?)<\/span>/s',$html,$poid);
			$product_id = $poid[1][0];
			
			/////////////////////////////////Product Image////////////////////////////////////
			   preg_match_all('/<img id="image" src="(.*?)"/s',$html,$resimage);

			   //preg_match_all('/<img src="(.*?)"/s',$resimage[1][0],$image_new);

			   $prod_thum_image = $resimage[1][0];

			//////////////////////////////////////////////////////////////////////////////
			
			$scrp_product_name = trim(addslashes($product_name));
			$scrp_product_desc =   substr($product_description, 0, 120);
			$scrp_product_image =$prod_thum_image;
			$js_scrp_product_price = $js_product_price;
			$scrp_product_price = $product_price;
			$scrp_product_id = $product_id;
			$scrp_landing_url = $loc;
			
			
			echo "var s25_scrp_product_name='".$scrp_product_name."';";
			if($scrp_product_price != "")
			{
				echo "var s25_scrp_product_price='".$scrp_product_price."';";
				echo "var s25_js_scrp_product_price='".$js_scrp_product_price."';";
			}
			else
			{
				echo "var s25_scrp_product_price='0';";
			}
			if($scrp_product_desc != "")
			{
				
				
				$temp_desc = nl2br($scrp_product_desc);
				$temp_desc = trim($temp_desc);
				
				$block = array('<br>', '<br />', '<br /><br />', '<br /> ', ' <br /> ','<br /> ');
				$pp_desc = str_replace($block," ",$temp_desc);
				$ppp_desc = preg_replace('/\s\s+/', ' ',$pp_desc);

				$pppp_desc = substr($ppp_desc, 0, 120);
				$pppp_desc.="...";

				echo "var s25_scrp_product_desc='".trim(addslashes($pppp_desc))."';";
				
			 }
			echo "var s25_scrp_product_image='".trim($scrp_product_image)."';";

			echo "var s25_scrp_product_id='".trim($scrp_product_id)."';";
							
		}
		
	}//end function
?>