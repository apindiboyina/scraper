<?php

	if(($_REQUEST["loc"]!=""))
	{
		$jsCall =1;
		getConfirmationVars($jsCall,$_REQUEST['loc']);
	}//if CLose.

	function getConfirmationVars($jsCall, $loc,$rnd)
	{
		if($jsCall == '0')
		{
			$location = $loc;
			$location = str_replace("*", "/", $location);
			$location = str_replace("|", "?", $location);
			$location = str_replace("^", "&", $location);
			$location = str_replace(" ", "+", $location);

			// Read Location 
			$product_price = "";
			//$html = file_get_contents($location);
			
			$opts = array('http'=>array('header' => "User-Agent:SocialAnnexScraper/1.0\r\n"));
			$context = stream_context_create($opts);
			$html = file_get_contents($location,false,$context);			

			//////////////////////////////Product Price//////////////////////////////////////
			
			preg_match_all('/<span itemprop="price">(.*?)<\/span>/s',$html,$resultpp);
			$prod_price1 =  strip_tags($resultpp[1][0]);			
			$prod_price2 = preg_replace("/\s|&nbsp;|INR/",'',$prod_price1);
			$prod_price = trim($prod_price2);
			$prod_price4 = $prod_price;			
			$pos = strpos($prod_price4,"$" );
			
			if ($pos === false) {
			 $product_price = "$".$prod_price4;
			   
			} else {
				$product_price =  $prod_price4;
			}
			
			$j_price = ",";
			$j_price1 = str_replace($j_price,'',$product_price);
			$j_price2 = explode("$", $j_price1);
			$js_product_price = $j_price2[1];
			
			///////////////////////////Product Description//////////////////////////////////
			
			preg_match_all('/<meta property="og:description" content="(.*?)"\/>/s',$html,$resultdesc);
			$product_description1 = $resultdesc[1][0];	
			$product_description1 = str_replace("#",'',$product_description1);
			$product_description1 = str_replace("&amp;",'and',$product_description1);
			//$product_description1 = str_replace('&quot;','"',$product_description1);
			
			$product_description1 = str_replace("'","",$product_description1);
			
			if($product_description1 == "")
			{
				$product_description1 = "CafePress.com is the flagship brand of CafePress Inc. Its where the world turns for unique products that express what people love most (on average, some 135,000 new designs are added each week). From the latest pop culture phenomenons and political scandals to favorite hobbies, activities, causes and interests, it's easy to find one-of-a-kind designs and merchandise at CafePress.com.";
			}
			
			$product_description=$product_description1;
			
			/////////////////////////////////Product Id////////////////////////////////////	
			
			preg_match_all('/<span id="pdp-value-productid">(.*?)<\/span>/s',$html,$resultid);		
			$product_id = $resultid[1][0];

			/////////////////////////////Product Image Path//////////////////////////////////
			/*
			$opts = array('http'=>array('header' => "User-Agent:SocialAnnexScraper/1.0\r\n"));
				$context = stream_context_create($opts); 
				$html1 = file_get_contents($location,false,$context);
				*/
				preg_match_all('/<meta property="og:image" content="(.*?)"/s',$html,$resultsi);	
					$original_image_path = $resultsi[1][0];
					
			if($original_image_path=='')
			{
				preg_match_all('/<img itemprop="image" alt=".*?" format=".*?" src="(.*?)"/s',$html,$resultalternateurl);
				$image_url=$resultalternateurl[1][0];
			}
			else
			{
			
					$image_array=explode('?', $original_image_path);
					$single_image = str_replace('product_zoom','product',$image_array[0]);
					if($single_image=="" || $single_image=="http://content.cafepress.com/global/img/cafepress-us.gif")
					{
						$poster_img = explode('+', $location);
						$poster_img1 = explode(',',$poster_img[1]);
						$poster_img_name = $poster_img1[0];
						$poster_img_id = $poster_img1[1];
						
						if (strpos($poster_img_id,'?') !== false) {
							/*$poster_img_id_array = explode('?', $poster_img_id);
						    //$new_poster_img_id=strstr($poster_img_id, '?', true);
						    $poster_img_id=trim($poster_img_id_array[0]);*/
						    $poster_img_id=trim($product_id);
						}
						
						$poster_original_img = "http://i1.cpcache.com/product/".$poster_img_id."/".$poster_img_name.".jpg";
						if($poster_img_name=="" || $poster_img_id=="")
						{
							$single_image = 'http://cdn.socialannex.com/custom_images/5788871/2TVT2L_PXFHFK_logo.png';
						}
						else
						{
							$single_image = $poster_original_img;
						}
					}
			
					/*preg_match_all('/<meta property="og:image" content="(.*?)"\/>/s',$html,$resultid);	
					$original_image_path = $resultid[1][0];*/
					$image_array=array();
					$image_array_parameter=array();
					$image_url='';
					$original_image_path=$single_image;
					if(!empty($original_image_path)){
						$image_array=explode('?', $original_image_path);
						if(!empty($image_array[1])){
							$image_array_parameter=explode('&', $image_array[1]);
							if(strstr($image_array_parameter[0],"color")!=false){
								$color=$image_array_parameter[0];
							}
							else{
								$color='color=white';
							}
							$image_url=$image_array[0]."?".$color."&height=460&width=460&".$image_array_parameter[3];
						}
						else{
							$image_url=$image_array[0];
						}
						
					}
		
			}
			$prod_thum_image = $image_url;
			
			//////////////////Product url and Product name//////////////////////////
			
			preg_match_all('/<div class="breadcrumb">(.*?)<\/div>/s',$html,$resultpurl);	
			$product_url_array=explode('&gt;', trim($resultpurl[1][0]));
			$main_url1 = $product_url_array[1];
			preg_match_all('/<a href="(.*?)"/s',$main_url1,$resultpurl1);
			$product_url=$resultpurl1[1][0];
			$main_product_array=explode('+', $product_url);
			$product_name=trim($main_product_array[2]);
			
			//////////////////Product Point url//////////////////////////
			
			preg_match_all('/<meta property="og:url" content="(.*?)"\/>/s',$html,$resultppurl);	
			$product_point_url =  $resultppurl[1][0];
			
			if($product_point_url==''){
				$product_point_url=$location;
			}

			///////////////////////////////////////////////////////////////////////////////

			$scrp_product_name = trim($product_name);
			$scrp_product_desc = substr($product_description, 0, 120);
			$scrp_product_image = $prod_thum_image;
			$js_scrp_product_price = $js_product_price;
			$scrp_product_price = $product_price;
			$scrp_product_id = $product_id;
			$scrp_landing_url = $product_url;
			$scrp_point_url = $product_point_url;
			
			
			$build = $scrp_product_name."^".$scrp_product_desc."^".$scrp_product_image."^".$scrp_point_url."^".$scrp_product_id."^".$scrp_landing_url;
			return $build; 
 
			
		}
		if($jsCall != '0')
		{ 
?>
		var loca = document.location.href;
		var scrp_custom_var	= loca;

		var scrp_product_name	= '';
		var scrp_product_desc	= '';
		var scrp_product_image	= '';
		var scrp_product_price	= '';
		var scrp_product_id		= '';
		var scrp_landing_url	= '';
		var js_scrp_product_price = '1';
<?php
		}//if
	}//end funciton
?>