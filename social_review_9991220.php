<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');

$location = $_REQUEST["loc"];

$location = str_replace("*", "/", $location);
$location = str_replace("|", "?", $location);
$location = str_replace("^", "&", $location);


// Read Location 
$product_price = "";

function get_domain($url) {
    $pieces = parse_url($url);
    $domain = isset($pieces['host']) ? $pieces['host'] : '';
    if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
        return $regs['domain'];
    }
    return false;
}

$domain_name = get_domain($location); // outputs 'somedomain.co.uk'

if ($domain_name == "600social.com") {
    $username = 'social';
    $password = 'social1234';

    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Basic " . base64_encode("$username:$password")
        )
    ));
    $html = file_get_contents($location, false, $context);
} else {
    $username = 'vionicshoes';
    $password = 'espresso';

    $context = stream_context_create(array(
        'http' => array(
            'header' => "Authorization: Basic " . base64_encode("$username:$password")
        )
    ));
    $html = scrapeContentUsingCurl($location,$username,$password);
}

function scrapeContentUsingCurl($productPageUrl,$username,$password) {
    $curl = curl_init($productPageUrl);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($curl, CURLOPT_USERPWD, $username . ":" . $password); //Your credentials goes here
    $pageContent = curl_exec($curl);
    if (curl_errno($curl)) { // check for execution errors
        echo 'Scraper error: ' . curl_error($curl);
        exit;
    }
    curl_close($curl);
    return $pageContent;
}

//////////////////////////////Product Name//////////////////////////////////////

preg_match_all('/<h1>(.*?)<\/h1>/s', $html, $result);

$product_name_1 = trim($result[1][0]);

$product_name = $product_name_1;

//////////////////////////////Product Price//////////////////////////////////////
if (preg_match_all('/<span class="product-price-regular">(.*?)<\/span>/s', $html, $resultprice)) {
    $js_product_price1 = trim($resultprice[1][0]);
    $js_product_price2 = explode("$", $js_product_price1);
    $js_product_price = $js_product_price2[1];
} else {
    preg_match_all('/<div class="price-info">(.*?)<\/div>/s', $html, $resultprice);

    preg_match_all('/<span class="regular-price" .*?>(.*?)<\/span>/s', $resultprice[1][0], $resultprice1);
    $product_id = $resultprice[1][0];
    $js_product_price1 = strip_tags($product_id);
    $js_product_price2 = explode("$", $js_product_price1);
    $js_product_price = trim($js_product_price2[1]);
}




///////////////////////////Product Description//////////////////////////////////

preg_match_all('/<meta name="description" content="(.*?)"/s', $html, $resultdesc);
$prod_desc1 = $resultdesc[1][0];
$product_description = trim($prod_desc1);
if ($product_description == "") {
    $product_description = "Shop for Orthaheel shoes, sandals, orthotics at VionicShoes.com. Get relief from sore knees and back. Stop suffering from plantar fasciitis today";
}
$product_description = substr($product_description, 0, 120) . "...";

/////////////////////////////Product Image Path//////////////////////////////////



if (preg_match_all('/<img class="cloudzoom" src="(.*?)"/s', $html, $resulti)) {
    $prod_image = $resulti[1][0];
} else {
    preg_match_all('/<img id="image-main".*?src="(.*?)"/s', $html, $resulti);
    $prod_image = $resulti[1][0];
}
$prod_image = str_replace("dev.vionicshoes.com", "d2uu5jtehvxu6t.cloudfront.net", $prod_image);

/////////////////////////////////Product Id////////////////////////////////////		
if (preg_match_all('/<div class="product-sku">(.*?)<\/div>/s', $html, $resultid)) {
    $product_ids = $resultid[1][0];
    $product_id1 = strip_tags($product_ids);
    $product_id2 = explode(":", $product_id1);
    $product_id = trim($product_id2[1]);
} else {
    preg_match_all('/productId : "(.*?)"/s', $html, $resultid);
    $product_id = trim($resultid[1][0]);
}

///////////////////////////////////////////////////////////////////////////////

echo "var s28_scrp_product_name ='" . $product_name . "';";
echo "var s28_scrp_product_desc ='" . $product_description . "';";
echo "var s28_scrp_product_image ='" . $prod_image . "';";
echo "var s28_scrp_product_price ='" . $js_product_price . "';";
echo "var s28_scrp_product_id ='" . $product_id . "';";
?>