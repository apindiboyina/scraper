<?php
			header('Access-Control-Allow-Origin: *');
			header('Access-Control-Allow-Methods: GET, POST');
			$location = $_REQUEST["loc"];

			$location = str_replace("*", "/", $location);
			$location = str_replace("|", "?", $location);
			$location = str_replace("^", "&", $location);
			$location = str_replace("~", "+", $location);


			// Read Location 
			$product_price = "";
			function get_domain($url)
			{
			  $pieces = parse_url($url);
			  $domain = isset($pieces['host']) ? $pieces['host'] : '';
			  if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
				return $regs['domain'];
			  }
			  return false;
			}

			$domain_name = get_domain($location); // outputs 'somedomain.co.uk'
			
			if($domain_name == "600social.com")
			{
				$username = 'social';
				$password = 'social1234';

				$context = stream_context_create(array(
				'http' => array(
				'header'  => "Authorization: Basic " . base64_encode("$username:$password")
				)
				));
				$html = file_get_contents($location,false,$context);
			}
			else{
				$opts = array('http'=>array('header' => "User-Agent:SocialAnnexScraper/1.0\r\n"));
			$context = stream_context_create($opts); 
			$html = file_get_contents($location,false,$context);
			}

		
//////////////////////////////Product Price//////////////////////////////////////
			
			if(preg_match_all('/<span itemprop="price">(.*?)<\/span>/s',$html,$resultpp))
			{
				$prod_price1 =  strip_tags($resultpp[1][0]);			
				$prod_price2 = preg_replace("/\s|&nbsp;|INR/",'',$prod_price1);
				$prod_price = trim($prod_price2);
				$prod_price4 = $prod_price;			
				$pos = strpos($prod_price4,"$" );
				
				if ($pos === false) {
				 $product_price = "$".$prod_price4;
				   
				} else {
					$product_price =  $prod_price4;
				}
				
				$j_price = ",";
				$j_price1 = str_replace($j_price,'',$product_price);
				$j_price2 = explode("$", $j_price1);
				$js_product_price = $j_price2[1];
			}
			elseif(preg_match_all('/<span id="salePriceId" class="redBoldText saleprice">(.*?)<\/span>/s',$html,$resultpp))
			{
				$prod_price1 =  strip_tags($resultpp[1][0]);			
				$prod_price2 = preg_replace("/\s|&nbsp;|INR/",'',$prod_price1);
				$prod_price = trim($prod_price2);
				$prod_price4 = $prod_price;			
				$pos = strpos($prod_price4,"$" );
				
				if ($pos === false) {
				 $product_price = "$".$prod_price4;
				   
				} else {
					$product_price =  $prod_price4;
				}
				
				$j_price = ",";
				$j_price1 = str_replace($j_price,'',$product_price);
				$j_price2 = explode("$", $j_price1);
				$js_product_price = $j_price2[1];
			}
			///////////////////////////Product Description//////////////////////////////////
			
			preg_match_all('/<meta property="og:description" content="(.*?)"\/>/s',$html,$resultdesc);
			$product_description1 = $resultdesc[1][0];	
			$product_description1 = str_replace("#",'',$product_description1);
			$product_description1 = str_replace("&amp;",'and',$product_description1);
			
			$product_description1 = str_replace("'","",$product_description1);
			
			if($product_description1 == "")
			{
				$product_description1 = "CafePress.com is the flagship brand of CafePress Inc. Its where the world turns for unique products that express what people love most (on average, some 135,000 new designs are added each week). From the latest pop culture phenomenons and political scandals to favorite hobbies, activities, causes and interests, it's easy to find one-of-a-kind designs and merchandise at CafePress.com.";
			}
			
			$product_description=$product_description1;
			
			/////////////////////////////////Product Id////////////////////////////////////	
			
			if(preg_match_all('/<span id="pdp-value-productid">(.*?)<\/span>/s',$html,$resultid))
			{
				$product_id = $resultid[1][0];
			}
			else{
				$product_id="noproductinfo";
			}

			/////////////////////////////Product Image Path//////////////////////////////////
			
			if(preg_match_all('/<meta property="og:image" content="(.*?)"\/>/s',$html,$resultid))
			{
				$original_image_path = $resultid[1][0];
				$image_array=array();
				$image_array_parameter=array();
				$image_url='';
				if(!empty($original_image_path)){
					$image_array=explode('?', $original_image_path);
					if(!empty($image_array[1])){
						$image_array_parameter=explode('&', $image_array[1]);
						if(strstr($image_array_parameter[0],"color")!=false){
							$color=$image_array_parameter[0];
						}
						else{
							$color='color=white';
						}
						$image_url=$image_array[0]."?".$color."&height=460&width=460&".$image_array_parameter[3];
					}
					else{
						$image_url=$image_array[0];
					}
					
				}
				$prod_thum_image = $image_url;
			}
			elseif(preg_match_all('/<div class="qv-design">(.*?)<\/div>/s',$html,$resultpurl))
			{
				
				preg_match_all('/<img class="qv-design-img".*?hoversrc="(.*?)"/s',$html,$result);
				$resultimage1 = explode('?',$result[1][0]);
				$prod_thum_image = strip_tags($resultimage1[0]);
			}

			//////////////////Product url and Product name//////////////////////////
			
			if(preg_match_all('/<h1 itemprop="name">(.*?)<\/h1>/s',$html,$resultpurl))
			{
				$product_name=trim($resultpurl[1][0]);
			}
			elseif(preg_match_all('/<div class="qv-details">(.*?)<\/div>/s',$html,$resultpurl))
			{
				preg_match_all('/<h3>(.*?)<\/h3>/s',$resultpurl[0][0],$pro_name);
				$product_name=strip_tags($pro_name[1][0]);
			}

			//////////////////////////////////////////////////////////////////////////////
			

			$scrp_product_name = trim($product_name);
			$scrp_product_desc = substr($product_description, 0, 90);
			$scrp_product_image = $prod_thum_image;
			$js_scrp_product_price = $js_product_price;
			$scrp_product_price = $product_price;
			$scrp_product_id = $product_id;
			$scrp_landing_url = $loc;

			echo "var scrp_product_name='".$scrp_product_name."';";

			echo "var scrp_product_desc='".$scrp_product_desc."';";

			echo "var scrp_product_image='".$scrp_product_image."';";

			echo "var js_scrp_product_price='".$js_scrp_product_price."';";

			echo "var scrp_product_price='".$scrp_product_price."';";

			echo "var scrp_product_id='".$scrp_product_id."';";

			echo "var scrp_landing_url='".$scrp_landing_url."';";
			

		
	
?>