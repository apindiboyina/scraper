<?php
if (($_REQUEST["loc"] != "")) {
    $jsCall = 1;
    getConfirmationVars($jsCall, $loc);
}//if CLose.

function getConfirmationVars($jsCall, $loc) {
    if ($jsCall == '0') {

        $location = $loc;

        $location = str_replace("*", "/", $location);
        $location = str_replace("|", "?", $location);
        $location = str_replace("^", "&", $location);

        // Read Location 
        $product_price = "";
        $html = file_get_contents($location);

        //////////////////////////////Product Name//////////////////////////////////////

        if (preg_match_all('/<h1 itemprop="name">(.*?)<\/h1>/s', $html, $prod_name)) {
            $prodname = trim($prod_name[1][0]);
            $product_name = $prodname;
        } else if (preg_match_all('/<title>(.*?)<\/title>/s', $html, $result)) {
            $product_name_1 = trim($result[1][0]);
            $product_name = $product_name_1;
        }

        //////////////////////////////Product Price//////////////////////////////////////
        if (preg_match_all('/<span class="price-sale" itemprop="price">(.*?)<\/div>/s', $html, $resultp)) {
            $prod_price1 = trim($resultp[1][0]);
            $prod_price2 = str_replace("<span>", "", $prod_price1);
            $prod_price3 = str_replace("</span>", "", $prod_price2);
            $prod_price4 = str_replace('<div class="clr">', '', $prod_price3);
            $product_price = $prod_price4;

            $j_price = ",";
            $j_price1 = $product_price;
            $j_price2 = explode("$", $j_price1);
            $js_product_price = $j_price2[1];
        } else if (preg_match_all('/<span class="price-sale"><sup>(.*?)<\/sup>(.*?)<\/span>/i', $html, $price)) {
            $product_price = $price[2][0];
            $j_price = ",";
            $j_price1 = $product_price;
            $j_price2 = explode("$", $j_price1);
            $js_product_price = $j_price2[1];
        }

        ///////////////////////////Product Description//////////////////////////////////

        preg_match_all('/<meta name="Description" content="(.*?)" \/>/s', $html, $resultdes);

        $product_desc11 = strip_tags($resultdes[1][0]);
        $replacechar = array('!', ';', ',', '-');
        $product_desc = str_replace($replacechar, " ", $product_desc11);
        $product_description = $product_desc;
        if ($product_description == "") {
            $product_description = "Manufacturers of Pure Hawaiian Spirulina Pacifica and BioAstin Natural Astaxanthin. We are located on the Kona coast of Hawaii.";
        }

        /////////////////////////////////Product Id////////////////////////////////////

        if (preg_match_all('/<input type="hidden" id="revProdId" name="revProdId" value="(.*?)"/s', $html, $resultpid)) {
            $pid1 = $resultpid[1][0];
            $product_id = $pid1;
        } else if (preg_match_all('/<input type="hidden" name="prod" id="prod" value="(.*?)" \/>/s', $html, $prod_id)) {
            $pid1 = $prod_id[1][0];
            $product_id = $pid1;
        }

        /////////////////////////////Product Image Path//////////////////////////////////

        /* preg_match_all('/<h1 itemprop="name">(.*?)<\/h1>/s', $html, $prodnm);
        preg_match_all('/<img id="mainProdImg" class="prod-img" itemprop="image" title="' . $prodnm[1][0] . '" src="(.*?)"/s', $html, $imgresult);
        $final_img_path = str_replace(array('&w=192', '&h=378'), array('&w=100', '&h=100'), $imgresult[1][0]);
        $prod_thum_image = "http://cdn.socialannex.com/custom_images/2209110/YLBOA5_100_100.png"; */
        
        /*preg_match_all('/<ul id="prodImgs">(.*?)<\/ul>/s', $html, $resultimg1);
        $img2 = $resultimg1[1][0];
        $prod_thum_image1 = $img2;
        preg_match_all("/<img .*?(?=src)src=\"([^\"]+)\"/si", $prod_thum_image1, $prod_thum_image2);
        $prod_thum_image3 = str_replace(array('&w=192', '&h=378','&w=320'), array('&w=100', '&h=100','&w=100'), $prod_thum_image2[1][0]);
        $prod_thum_image = $prod_thum_image3;*/
		
		/* preg_match_all('/<ul id="prodImgs">(.*?)<\/ul>/s', $html, $resultimg1);
        $img2 = $resultimg1[1][0];
        $prod_thum_image1 = $img2;
        preg_match_all("/<img .*?(?=src)src=\"([^\"]+)\"/si", $prod_thum_image1, $prod_thum_image2);
        $prod_thum_image3 = str_replace(array('&w=192', '&h=378','&w=320'), array('&w=100', '&h=100','&w=100'), $prod_thum_image2[1][0]);
        $prod_thum_image4 = $prod_thum_image3;
  $prod_thum_image=str_replace("path=","path=http://nutrexhawaii.cachefly.net/itemimages/",$prod_thum_image4);*/
  
  
  		 preg_match_all('/<ul id="prodImgs">(.*?)<\/ul>/s', $html, $resultimg1);
        $img2 = $resultimg1[1][0];
        $prod_thum_image1 = $img2;
        preg_match_all("/<img .*?(?=src)src=\"([^\"]+)\"/si", $prod_thum_image1, $prod_thum_image2);
        $prod_thum_image3 = str_replace(array('&w=192', '&h=378','&w=320'), array('&w=100', '&h=100','&w=100'), $prod_thum_image2[1][0]);
	  
	   $prod_thum_image3=  explode("=",$prod_thum_image3);
       $prod_thum_image4 = $prod_thum_image3[2];
	   $prod_thum_image5 = explode("&",$prod_thum_image4);
	 
     $prod_thum_image="http://nutrexhawaii.cachefly.net/itemimages/".$prod_thum_image5[0];

        ///////////////////////////////////////////////////////////////////////////////////
        $scrp_product_name = trim($product_name);
        $scrp_product_desc = substr($product_description, 0, 70)."...";
        $scrp_product_image = $prod_thum_image;
        $js_scrp_product_price = $js_product_price;
        $scrp_product_price = $product_price;
        $scrp_product_id = $product_id;
        $scrp_landing_url = $loc;


        $build = $scrp_product_name . "^" . $scrp_product_desc . "^" . $scrp_product_image . "^" . $scrp_product_price . "^" . $scrp_product_id . "^" . $scrp_landing_url;

        return $build;
    }
    if ($jsCall != '0') {
        ?>

        var loca = document.location.href;
        var scrp_custom_var	= loca;

        var scrp_product_name	= '';
        var scrp_product_desc	= '';
        var scrp_product_image	= '';
        var scrp_product_price	= '';
        var scrp_product_id		= '';
        var scrp_landing_url	= '';
        var js_scrp_product_price = '1';
        <?php
    }//if
}

//end funciton
?>