<?php
			header('Access-Control-Allow-Origin: *');
			header('Access-Control-Allow-Methods: GET, POST');

			$location = $_REQUEST["loc"];

			$location = str_replace("*", "/", $location);
			$location = str_replace("|", "?", $location);
			$location = str_replace("^", "&", $location);


			// Read Location 
			$product_price = "";

			$html = file_get_contents($location);

		
			
			//////////////////////////////Product Name//////////////////////////////////////
			preg_match_all('/<meta property="og:title" content="(.*?)"/s',$html,$result);
			$product_name_13 =  trim($result[1][0]);
			$product_name = $product_name_13;
			
			///////////////////////////////Left Side Title/////////////////////////////////

			preg_match_all('/<div class="artist left">(.*?)<\/div>/s',$html,$resultname1);
			//print_r($resultname1[1][0]);exit;
			preg_match_all('/<span class="shirt_title">(.*?)<\/span>/s',$resultname1[1][0],$prod_1);
			$left_product=$prod_1[1][0];
			
			preg_match_all('/<span class="by_link_left">(.*?)<\/span>/s',$resultname1[1][0],$prodauthor_1);
			$left_author=strip_tags($prodauthor_1[1][0]);
			
			$product_name1=$left_product." ".$left_author;
			///////////////////////////////End Left Side Title/////////////////////////////////
			///////////////////////////////Right Side Title/////////////////////////////////
			preg_match_all('/<div class="artist right">(.*?)<\/div>/s',$html,$resultname2);
			//print_r($resultname2[1][0]);exit;
			preg_match_all('/<span class="shirt_title">(.*?)<\/span>/s',$resultname2[1][0],$prod_2);
			$right_product=$prod_2[1][0];
			
			preg_match_all('/<span class="by_link_right">(.*?)<\/span>/s',$resultname2[1][0],$prodauthor_2);
			$right_author=strip_tags($prodauthor_2[1][0]);
			
			$product_name2=$right_product." ".$right_author;
			//////////////////////////////Product Price//////////////////////////////////////
			$product_price= '1';
			$js_product_price= '1';
			
			$product_price1 = "1";	
			$js_product_price1 = "1";
			
			$product_price2 = "1";	
			$js_product_price2 = "1";

			///////////////////////////Product Description//////////////////////////////////
			
			$product_description ="Check out my new favorite tee from TeeFury!";

			if($product_description == "")
			{
				$product_description = "Wear your art on your sleeve with 24-hour limited edition tees for only $11 from TeeFury. Buy today's tee and vote your favorite designs back from the dead.";
			}
			
			$product_description1 = "Wear your art on your sleeve with 24-hour limited edition tees for only $11 from TeeFury.";

			$product_description2 = "Wear your art on your sleeve with 24-hour limited edition tees for only $11 from TeeFury.";
			/////////////////////////////Product Image Path//////////////////////////////////
			
			preg_match_all('/<div id="zoom-pic" class="main-artwork-image-container" style="(.*?)">(.*?)<\/div>/s',$html,$resulti);
		   
			$prod_image =$resulti[0][0];
			$prod_image2 = explode('<img src="',$prod_image);
			$prod_image3 = explode('"',$prod_image2[1]);
			$prod_image4=$prod_image3[0];
			$prod_thum_image = str_replace("products_large_images","products_large_images_flat",$prod_image4);
			
			if($prod_thum_image=="")
			{
				$prod_thum_image="http://cdn.socialannex.com/custom_images/9914030/5R4JTQ_NVK74Q_tee.png";
			}
			
			preg_match_all('/<div class="colors-tst-left-img-container".*?><img src="(.*?)"/s',$html,$resulti1);
			$prod_image1 =$resulti1[0][0];
			$prod_image21 = explode('<img src="',$prod_image1);
			$prod_image31 = explode('"',$prod_image21[1]);
			$prod_image41=$prod_image31[0];
			$prod_thum_image1 = str_replace("products_large_images","products_large_images_flat",$prod_image41);
			
			preg_match_all('/<div class="colors-tst-right-img-container".*?><img src="(.*?)"/s',$html,$resulti2);
			$prod_image2 =$resulti2[0][0];
			$prod_image22 = explode('<img src="',$prod_image2);
			$prod_image32 = explode('"',$prod_image22[1]);
			$prod_image42=$prod_image32[0];
			$prod_thum_image2 = str_replace("products_large_images","products_large_images_flat",$prod_image42);

			/////////////////////////////////Product Id////////////////////////////////////
			$product_id = "noproductinfo";
			
			/////////////////////////////////TwoFury Product Ids/////////////////////////
			
			preg_match_all('/<input type="hidden" name="product_id_a" id="product_id_a" value="(.*?)"/s',$html,$resultid1);
			$product_id11 = $resultid1[1][0];
			$product_id1=$product_id11;
			
			preg_match_all('/<input type="hidden" name="product_id_b" id="product_id_b" value="(.*?)"/s',$html,$resultid2);
			$product_id12 = $resultid2[1][0];
			$product_id2=$product_id12;
			///////////////////////////////////////////////////////////////////////////////

			$scrp_product_name = trim($product_name);
			$scrp_product_desc = substr($product_description, 0, 90);
			$scrp_product_image = $prod_thum_image;
			$js_scrp_product_price = $js_product_price;
			$scrp_product_price = $product_price;
			$scrp_product_id = $product_id;
			$scrp_caption = 'www.teefury.com';
			
			$scrp_product_name1 = trim($product_name1);
			$scrp_product_desc1 = substr($product_description1, 0, 90);
			$scrp_product_image1 = $prod_thum_image1;
			$js_scrp_product_price1 = $js_product_price1;
			$scrp_product_price1 = $product_price1;
			$scrp_product_id1 = $product_id1;
			if($scrp_product_desc1 != "")
			{
				$product_short_desc1 = substr($scrp_product_desc1, 0,50);
				$product_short_desc1.="...";
				echo "var s12_scrp_product_short_desc1='".trim(addslashes($product_short_desc1))."';";
							
			}
			
			$scrp_product_name2 = trim($product_name2);
			$scrp_product_desc2 = substr($product_description2, 0, 90);
			$scrp_product_image2 = $prod_thum_image2;
			$js_scrp_product_price2 = $js_product_price2;
			$scrp_product_price2 = $product_price2;
			$scrp_product_id2 = $product_id2;
			
			if($scrp_product_desc2 != "")
			{
			$product_short_desc2 = substr($scrp_product_desc2, 0,50);
			$product_short_desc2.="...";
			echo "var s12_scrp_product_short_desc2='".trim(addslashes($product_short_desc2))."';";
							
			}

			echo "var scrp_product_name='".$scrp_product_name."';";

			echo "var scrp_product_desc='".$scrp_product_desc."';";

			echo "var scrp_product_image='".$scrp_product_image."';";

			echo "var js_scrp_product_price='".$js_scrp_product_price."';";

			echo "var scrp_product_price='".$scrp_product_price."';";

			echo "var scrp_product_id='".$scrp_product_id."';";

			echo "var scrp_landing_url='".$scrp_landing_url."';";
			
			
			
			echo "var s12_scrp_product_name1='".$scrp_product_name1."';";

			echo "var s12_scrp_product_desc1='".$scrp_product_desc1."';";

			echo "var s12_scrp_product_image1='".$scrp_product_image1."';";

			echo "var s12_js_scrp_product_price1='".$js_scrp_product_price1."';";

			echo "var s12_scrp_product_price1='".$scrp_product_price1."';";

			echo "var s12_scrp_product_id1='".$scrp_product_id1."';";
			
			
			echo "var s12_scrp_product_name2='".$scrp_product_name2."';";

			echo "var s12_scrp_product_desc2='".$scrp_product_desc2."';";

			echo "var s12_scrp_product_image2='".$scrp_product_image2."';";

			echo "var s12_js_scrp_product_price2='".$js_scrp_product_price2."';";

			echo "var s12_scrp_product_price2='".$scrp_product_price2."';";

			echo "var s12_scrp_product_id2='".$scrp_product_id2."';";	

?>