<?php
	ini_set("display_errors", 0); 

	if(($_REQUEST["loc"]!=""))
	{
		$jsCall =1;
		$loc = $_REQUEST["loc"];
		getConfirmationVars($jsCall,$loc);
	}//if CLose.

	function getConfirmationVars($jsCall,$loc)
	{
		if($jsCall != '0')
		{ 
			$location = $loc;

			$location = str_replace("*", "/", $location);
			$location = str_replace("|", "?", $location);
			$location = str_replace("^", "&", $location);

			// Read Location 
			$product_price = "";
			$html = file_get_contents($location);
			
			//////////////////////////////Product Name//////////////////////////////////////

			preg_match_all('/<title>(.*?)<\/title>/s',$html,$result);

			$product_name_1 =  trim($result[1][0]);			
			
			$product_name_2 = str_replace('"','',$product_name_1);

			$product_name = $product_name_2;			

			//////////////////////////////Product Price//////////////////////////////////////

			preg_match_all('/<strong style="font: bold 16px verdana;">(.*?)<\/strong>/s',$html,$resultp);
			
			$prod_price1 =  strip_tags($resultp[1][0]);			

			$product_price =  $prod_price1;

	
			///////////////////////////Product Description//////////////////////////////////

			preg_match_all('/<div itemprop="description">(.*?)<\/div>/s',$html,$result);

			$product_description = strip_tags($result[1][0]);

			$product_description = str_replace("'","",$product_description1);
			
			if($product_description == "")
			{
				$product_description = "This Classic Superman shirt will turn its wearer into the Man of Steel. The Superman shield is screen printed across the chest. The approximate measurement of the shield is 13 inches wide by 9.5 inches long.";
			}

			/////////////////////////////Product Image Path//////////////////////////////////

			preg_match_all('/<div align="center">(.*?)<\/div>/s',$html,$resulti);	
			
			$prod_image = $resulti[0][0];

			preg_match('/<img(.*)src(.*)=(.*)"(.*)"/U', $prod_image, $resultImg);
							
			$prod_image = trim($resultImg[4]);
			
			$prod_thum_image = $prod_image;

			/////////////////////////////////Product Id////////////////////////////////////	
						
			preg_match_all('/<form method="post" name="(.*?)"/s',$html,$resulti);
			
			$prod_id2 = trim($resulti[1][0]);
			
			$prod_id2 = strip_tags($prod_id2);
			
			$product_id = $prod_id2;
			
			///////////////////////////////////////////////////////////////////////////////
		

			$scrp_product_name = addslashes(trim($product_name));
			$scrp_product_desc = substr($product_description, 0, 120);
			$scrp_product_image = $prod_thum_image;
			$scrp_product_price = $product_price;
			$scrp_product_id = $product_id;
			$scrp_landing_url = $loc;		

			echo "var scrp_product_name='".trim($scrp_product_name)."';";
			echo "var scrp_product_desc	='".trim($scrp_product_desc)."';";
			echo "var scrp_product_image='".trim($scrp_product_image)."';";
			echo "var scrp_product_price='".trim($scrp_product_price)."';";
			echo "var scrp_product_id='".trim($scrp_product_id)."';";

		}//if
	}//end funciton
?>