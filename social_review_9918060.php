<?php
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST');
			$location = $_REQUEST["loc"];
			$location = str_replace("*", "/", $location);
			$location = str_replace("|", "?", $location);
			$location = str_replace("^", "&", $location);


			// Read Location 
			$product_price = "";

			function get_domain($url)
			{
				$pieces = parse_url($url);
				$domain = isset($pieces['host']) ? $pieces['host'] : '';
				if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) 
				{
					return $regs['domain'];
				}
				return false;
			}

		/**	
		  @author GK001
		  Modified by 6/12/2015
		  Removed file_get_contents instead of url_get_contents function.	
		**/
		function url_get_contents($url) {			
			$sa_s28_ch = curl_init();
			curl_setopt($sa_s28_ch,CURLOPT_URL, $url);
			curl_setopt($sa_s28_ch,CURLOPT_RETURNTRANSFER,1);
			$result = curl_exec($sa_s28_ch); 
			curl_close($sa_s28_ch);
		   
		   return $result;
		 }

			$domain_name = get_domain($location); // outputs 'somedomain.co.uk'
		   
			if($domain_name == "600social.com")
			{
				$username = 'social';
				$password = 'social1234';

				$context = stream_context_create(array(
				'http' => array(
				'header'  => "Authorization: Basic " . base64_encode("$username:$password")
				)
				));
				$html = file_get_contents($location,false,$context);
			}
			else
			{
				//$html = file_get_contents($location);
				$html = url_get_contents($location);
			}

	//////////////////////////////Product Name//////////////////////////////////////

	/*preg_match_all('/<h1 itemprop="name">(.*?)<\/h1>/s',$html,$result);

	$product_name_1 =  trim($result[1][0]);		
	if($product_name==""){
		$product_name = "Tour Master Saber Series 3 Textile Jacket";
	}*/
	 $product_name = "Tour Master Saber Series 3 Textile Jacket";			

	//////////////////////////////Product Price//////////////////////////////////////

	/*$js_product_price1 = preg_match_all('/sa_p_price: "(.*?)"/s',$html,$resultprice);
	if($js_product_price==""){
	  $js_product_price = "143.99";
	}*/
	$js_product_price = "143.99";
	///////////////////////////Product Description//////////////////////////////////

	$res=preg_match_all('/<div id="pdTab1".*?>(.*?)<\/div>/s',$html,$resultdesc);
                                   
	$prod_desc1 = $resultdesc[1][0];
	$product_description = strip_tags(trim(addslashes($prod_desc1)));					
    if($product_description == "")
	{
		$product_description = "Exquisitely tailored and custom designed suits, shirts, blazers, trousers, tuxedos and accessories.  All made to measure, shipped to you free of charge and backed by our Flawless Fit Promise.";
	}
	$product_description=substr($product_description,0,120)."...";

	/////////////////////////////Product Image Path//////////////////////////////////
	preg_match_all('/sa_p_img_url: "(.*?)"/s',$html,$resulti);
	$prod_image =$resulti[1][0];
	if($prod_image==""){
		$prod_image="http://static.chaparral-racing.com/productimages/600/03-8774-0301-04.jpg";
	}

	/////////////////////////////////Product Id////////////////////////////////////	

		preg_match_all('/sa_p_id: "(.*?)"/s',$html,$resultid);
		$product_id = $resultid[1][0];
		
		if($product_id =="")
		{
			$product_id="25591494";
		}
	
	///////////////////////////////////////////////////////////////////////////////

		echo "var s28_scrp_product_name ='".trim(addslashes($product_name))."';";
		if($product_description != "")
		{
			$temp_desc = nl2br($product_description);
			$temp_desc = trim($temp_desc);
			
			$block = array('<br>', '<br />', '<br /><br />', '<br /> ', ' <br /> ','<br /> ');
			$pp_desc = str_replace($block," ",$temp_desc);
			$pp_desc1 = str_replace("&#39;","'",$pp_desc);
			
			$ppp_desc = preg_replace('/\s\s+/', ' ',$pp_desc1);

			$pppp_desc = substr($ppp_desc, 0, 120);
			$pppp_desc.="...";

			echo "var s28_scrp_product_desc='".trim(addslashes($pppp_desc))."';";
		 }
		echo "var s28_scrp_product_image ='".$prod_image."';";
		echo "var s28_scrp_product_price ='".$js_product_price."';";
        echo "var s28_scrp_product_id ='".$product_id."';";		
?>