<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');

			$location = $_REQUEST["loc"];

			$location = str_replace("*", "/", $location);
			$location = str_replace("|", "?", $location);
			$location = str_replace("^", "&", $location);


			// Read Location 
			$product_price = "";

			function get_domain($url)
			{
				$pieces = parse_url($url);
				$domain = isset($pieces['host']) ? $pieces['host'] : '';
				if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) 
				{
					return $regs['domain'];
				}
				return false;
			}

		/**	
		  @author GK001
		  Modified by 6/12/2015
		  Removed file_get_contents instead of url_get_contents function.	
		**/
		function url_get_contents($url) {			
			$sa_s28_ch = curl_init();
			curl_setopt($sa_s28_ch,CURLOPT_URL, $url);
			curl_setopt($sa_s28_ch,CURLOPT_RETURNTRANSFER,1);
			$result = curl_exec($sa_s28_ch); 
			curl_close($sa_s28_ch);
		   
		   return $result;
		 }

			$domain_name = get_domain($location); // outputs 'somedomain.co.uk'
		   
			if($domain_name == "600social.com")
			{
				$username = 'social';
				$password = 'social1234';

				$context = stream_context_create(array(
				'http' => array(
				'header'  => "Authorization: Basic " . base64_encode("$username:$password")
				)
				));
				$html = file_get_contents($location,false,$context);
			}
			else
			{
				//$html = file_get_contents($location);
				$html = url_get_contents($location);
			}

	//////////////////////////////Product Name//////////////////////////////////////

	/*
	preg_match_all('/<h1>(.*?)<\/h1>/s',$html,$result);

	$product_name_1 =  trim($result[1][0]);		
	*/
	$product_name = "A hiking boot that looks as good as it feels";			

	//////////////////////////////Product Price//////////////////////////////////////
	/*
	if(preg_match_all('/<span class="product-price-regular">(.*?)<\/span>/s',$html,$resultprice))
        { 
        $js_product_price1 =  trim($resultprice[1][0]);
	$js_product_price2 = explode("$",$js_product_price1);
	$js_product_price = $js_product_price2[1];
        }
        else
        {
        preg_match_all('/<div class="price-info">(.*?)<\/div>/s',$html,$resultprice);
        
        preg_match_all('/<span class="regular-price" .*?>(.*?)<\/span>/s',$resultprice[1][0],$resultprice1);
        $product_id = $resultprice[1][0];
       $js_product_price1 =  strip_tags($product_id);
	$js_product_price2 = explode("$",$js_product_price1);
	$js_product_price = trim($js_product_price2[1]);         
        }
    */              
		$js_product_price = "180.00"; 


	///////////////////////////Product Description//////////////////////////////////

preg_match_all('/<meta name="description" content="(.*?)"/s',$html,$resultdesc);
	$prod_desc1 = $resultdesc[1][0];
	$product_description = trim($prod_desc1);					
    if($product_description == "")
	{
		$product_description = "Built under the principles that your feet have all the technology you'll need, the TRACKER, part of our extreme series, equips you with the necessary tools for the job.";
	}
	$product_description=substr($product_description,0,120)."...";

	/////////////////////////////Product Image Path//////////////////////////////////
	

	/*
	   if(preg_match_all('/<img class="cloudzoom" src="(.*?)"/s',$html,$resulti))
           {
              $prod_image =$resulti[1][0];
           }
        else
           {
              preg_match_all('/<img id="image-main".*?src="(.*?)"/s',$html,$resulti);
              $prod_image =$resulti[1][0];
           }
	*/
	
// $prod_image = "http://www.vivobarefoot.com/userdata/root/images/aw14/tracker-mens/300047-01_mens_tracker_brown_side.jpg%7Bw=540%7D.th";

	$prod_image = "http://cdn.socialannex.com/custom_images/9411370/5BREIL_v_logo.png";
	
	/////////////////////////////////Product Id////////////////////////////////////		
	/*
	if(preg_match_all('/<div class="product-sku">(.*?)<\/div>/s',$html,$resultid))
        { 
	     $product_ids = $resultid[1][0];
             $product_id1= strip_tags($product_ids);
             $product_id2 = explode(":",$product_id1);
             $product_id = trim($product_id2[1]);
        }
        else
        {
             preg_match_all('/productId : "(.*?)"/s',$html,$resultid);
             $product_id =trim($resultid[1][0]);     
        }
	*/
	$product_id = "300047-01";
	
	///////////////////////////////////////////////////////////////////////////////

		echo "var s28_scrp_product_name ='".$product_name."';";
		echo "var s28_scrp_product_desc ='".$product_description."';";
		echo "var s28_scrp_product_image ='".$prod_image."';";
		echo "var s28_scrp_product_price ='".$js_product_price."';";
        echo "var s28_scrp_product_id ='".$product_id."';";		
?>