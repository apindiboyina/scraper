<?php
	if(($_REQUEST["loc"]!=""))
	{
		$jsCall =1;
		getConfirmationVars($jsCall,$loc);
	}//if CLose.

	function getConfirmationVars($jsCall,$loc)
	{
		if($jsCall == '0')
		{
			if($loc=='Atlanta')
			{
				$scrp_product_name	= "I just saved money with Atlanta CityPASS!";
				$scrp_product_desc	= "Atlanta CityPASS saves you 51% on 6 must-see Atlanta attraction admission tickets.";
				$scrp_product_image	= "http://www.citypass.com/img/atlanta/atlanta-booklet-cover.png";
				$scrp_product_price	= "";
				$scrp_product_id	= "Atlanta";
				$scrp_landing_url	= "http://www.citypass.com/atlanta?mv_source=socialannex";

			}
			if($loc=='New York')
			{
				$scrp_product_name	= "I just saved money with New York CityPASS!";
				$scrp_product_desc	= "New York CityPASS saves you 45% on 6 must-see New York attraction admission tickets.";
				$scrp_product_image	= "http://www.citypass.com/img/ny/ny-booklet-cover.png";
				$scrp_product_price	= "";
				$scrp_product_id	= "New York";
				$scrp_landing_url	= "http://www.citypass.com/new-york?mv_source=socialannex";
			}
			if($loc=='Boston')
			{
				$scrp_product_name	= "I just saved money with Boston CityPASS!";
				$scrp_product_desc	= "Boston CityPASS saves you 46% on 5 must-see Boston attraction admission tickets.";
				$scrp_product_image	= "http://www.citypass.com/img/boston/boston-booklet-cover.png";
				$scrp_product_price	= "";
				$scrp_product_id	= "Boston";
				$scrp_landing_url	= "http://www.citypass.com/boston?mv_source=socialannex";
			}
			if($loc=='Philadelphia')
			{
				$scrp_product_name	= "I just saved money with Philadelphia CityPASS!";
				$scrp_product_desc	= "Philadelphia CityPASS saves you 47% on 6 must-see Philadelphia attraction admission tickets.";
				$scrp_product_image	= "http://www.citypass.com/img/philadelphia/philadelphia-booklet-cover.png";
				$scrp_product_price	= "";
				$scrp_product_id	= "Philadelphia";
				$scrp_landing_url	= "http://www.citypass.com/philadelphia?mv_source=socialannex";
			}
			if($loc=='Chicago')
			{
				$scrp_product_name	= "I just saved money with Chicago CityPASS!";
				$scrp_product_desc	= "Chicago CityPASS saves you 49% on 5 must-see Chicago attraction admission tickets.";
				$scrp_product_image	= "http://www.citypass.com/img/chicago/chicago-booklet-cover.png";
				$scrp_product_price	= "";
				$scrp_product_id	= "Chicago";
				$scrp_landing_url	= "http://www.citypass.com/chicago?mv_source=socialannex";
			}
			if($loc=='San Francisco')
			{
				$scrp_product_name	= "I just saved money with San Francisco CityPASS!";
				$scrp_product_desc	= "San Francisco CityPASS saves you 49% on 5 must-see San Francisco attraction admission tickets.";
				$scrp_product_image	= "http://www.citypass.com/img/sanfrancisco/sanfrancisco-booklet-cover.png";
				$scrp_product_price	= "";
				$scrp_product_id	= "San Francisco";
				$scrp_landing_url	= "http://www.citypass.com/san-francisco?mv_source=socialannex";
			}
			if($loc=='Hollywood')
			{
				$scrp_product_name	= "I just saved money with Hollywood CityPASS!";
				$scrp_product_desc	= "Hollywood CityPASS saves you 43% on 4 must-see Hollywood attraction admission tickets.";
				$scrp_product_image	= "http://www.citypass.com/img/hollywood/hollywood-booklet-cover.png";
				$scrp_product_price	= "";
				$scrp_product_id	= "Hollywood";
				$scrp_landing_url	= "http://www.citypass.com/hollywood?mv_source=socialannex";
			}
			if($loc=='Seattle')
			{
				$scrp_product_name	= "I just saved money with Seattle CityPASS!";
				$scrp_product_desc	= "Seattle CityPASS saves you 47% on 6 must-see Seattle attraction admission tickets.";
				$scrp_product_image	= "http://www.citypass.com/img/seattle/seattle-booklet-cover.png";
				$scrp_product_price	= "";
				$scrp_product_id	= "Seattle";
				$scrp_landing_url	= "http://www.citypass.com/seattle?mv_source=socialannex";
			}
			if($loc=='Houston')
			{
				$scrp_product_name	= "I just saved money with Houston CityPASS!";
				$scrp_product_desc	= "Houston CityPASS saves you 47% on 6 must-see Houston attraction admission tickets.";
				$scrp_product_image	= "http://www.citypass.com/img/houston/houston-booklet-cover.png";
				$scrp_product_price	= "";
				$scrp_product_id	= "Houston";
				$scrp_landing_url	= "http://www.citypass.com/houston?mv_source=socialannex";
			}
			if($loc=='Toronto')
			{
				$scrp_product_name	= "I just saved money with Toronto CityPASS!";
				$scrp_product_desc	= "Toronto CityPASS saves you 50% on 5 must-see Toronto attraction admission tickets.";
				$scrp_product_image	= "http://www.citypass.com/img/toronto/toronto-booklet-cover.png";
				$scrp_product_price	= "";
				$scrp_product_id	= "Toronto";
				$scrp_landing_url	= "http://www.citypass.com/toronto?mv_source=socialannex";
			}
			if($loc=='Southern California')
			{
				$scrp_product_name	= "I just saved money with Southern California CityPASS!";
				$scrp_product_desc	= "Southern California CityPASS saves you 30% on admission to Southern California attractions and theme parks.";
				$scrp_product_image	= "http://www.citypass.com/img/citypass/logo-socal-refresh.jpg";
				$scrp_product_price	= "";
				$scrp_product_id	= "Southern California";
				$scrp_landing_url	= "http://www.citypass.com/southern-california?mv_source=socialannex";
			}

			$scrp_show_info = "noproductinfo";

			$build = $scrp_product_name."^".$scrp_product_desc."^".$scrp_product_image."^".$scrp_product_price."^".$scrp_product_id."^".$scrp_landing_url."^".$scrp_show_info;

			return $build; 		
		}
		if($jsCall != '0')
		{ 

?>

var xx   = $saobject('body').html();
	var html = xx.replace(/<\/?(?!\!)[^>]*>/gi, '');
 
	if( (html.search(/Atlanta/i)) != -1)
	{
		var scrp_custom_var		= 'Atlanta';
		var scrp_product_name	= '';
		var scrp_product_desc	= '';
		var scrp_product_image	= '';
		var scrp_product_price	= '';
		var scrp_product_id		= '';
		var scrp_landing_url	= '';
		var js_scrp_product_price = '1';
		
	}
	else if( (html.search(/New York/i)) != -1)
	{
		var scrp_custom_var		= 'New York';
		var scrp_product_name	= '';
		var scrp_product_desc	= '';
		var scrp_product_image	= '';
		var scrp_product_price	= '';
		var scrp_product_id		= '';
		var scrp_landing_url	= '';
		var js_scrp_product_price = '1';
	}
	else if( (html.search(/Boston/i)) != -1)
	{
		var scrp_custom_var		= 'Boston';
		var scrp_product_name	= '';
		var scrp_product_desc	= '';
		var scrp_product_image	= '';
		var scrp_product_price	= '';
		var scrp_product_id		= '';
		var scrp_landing_url	= '';
		var js_scrp_product_price = '1';
	}
	else if( (html.search(/Philadelphia/i)) != -1)
	{
		var scrp_custom_var		= 'Philadelphia';
		var scrp_product_name	= '';
		var scrp_product_desc	= '';
		var scrp_product_image	= '';
		var scrp_product_price	= '';
		var scrp_product_id		= '';
		var scrp_landing_url	= '';
		var js_scrp_product_price = '1';
	}
	else if( (html.search(/Chicago/i)) != -1)
	{
		var scrp_custom_var		= 'Chicago';
		var scrp_product_name	= '';
		var scrp_product_desc	= '';
		var scrp_product_image	= '';
		var scrp_product_price	= '';
		var scrp_product_id		= '';
		var scrp_landing_url	= '';
		var js_scrp_product_price = '1';
	}
	else if( (html.search(/San Francisco/i)) != -1)
	{
		var scrp_custom_var		= 'San Francisco';
		var scrp_product_name	= '';
		var scrp_product_desc	= '';
		var scrp_product_image	= '';
		var scrp_product_price	= '';
		var scrp_product_id		= '';
		var scrp_landing_url	= '';
		var js_scrp_product_price = '1';
	}
	else if( (html.search(/Hollywood/i)) != -1)
	{
		var scrp_custom_var		= 'Hollywood';
		var scrp_product_name	= '';
		var scrp_product_desc	= '';
		var scrp_product_image	= '';
		var scrp_product_price	= '';
		var scrp_product_id		= '';
		var scrp_landing_url	= '';
		var js_scrp_product_price = '1';
	} 
	else if( (html.search(/Seattle/i)) != -1)
	{
		var scrp_custom_var		= 'Seattle';
		var scrp_product_name	= '';
		var scrp_product_desc	= '';
		var scrp_product_image	= '';
		var scrp_product_price	= '';
		var scrp_product_id		= '';
		var scrp_landing_url	= '';
		var js_scrp_product_price = '1';
	}  
	else if( (html.search(/Houston/i)) != -1)
	{
		var scrp_custom_var		= 'Houston';
		var scrp_product_name	= '';
		var scrp_product_desc	= '';
		var scrp_product_image	= '';
		var scrp_product_price	= '';
		var scrp_product_id		= '';
		var scrp_landing_url	= '';
		var js_scrp_product_price = '1';
	}  
	else if( (html.search(/Toronto/i)) != -1)
	{
		var scrp_custom_var		= 'Toronto';
		var scrp_product_name	= '';
		var scrp_product_desc	= '';
		var scrp_product_image	= '';
		var scrp_product_price	= '';
		var scrp_product_id		= '';
		var scrp_landing_url	= '';
		var js_scrp_product_price = '1';
	}  
	else if( (html.search(/Southern California/i)) != -1)
	{
		var scrp_custom_var		= 'Southern California';
		var scrp_product_name	= '';
		var scrp_product_desc	= '';
		var scrp_product_image	= '';
		var scrp_product_price	= '';
		var scrp_product_id		= '';
		var scrp_landing_url	= '';
		var js_scrp_product_price = '1';
	}  
 

<?php
		}//if
	}//end funciton
?>

	